
%% reinstatement prior to startle (3rd day outside scanner only)



%% STARTSTOPSESSIONMARKER
if isLPT==1 ;  dio=diosend(dio,[AD.LPTsessionStart   5 ]);  end





%
%     STARTLE stuff v1.0
%

% ==========================================
%% [1] make startle-TABLE
% ==========================================

%% copy first 4 trials (ordered trials) from Table
tbstartle=x.tb_backup(1:4,:);
% tbstartle=[repmat(tbstartle,[2 1]); tbstartle(1:2,:) ];
tbstartle=[repmat(tbstartle,[2 1]);  ];
tbstartle=sortrows(tbstartle,[6]); %sort accord. condition

%% add and balance ISI-time
ITIst=sort(x.tb_backup(:,7));
ITIst=prctile(ITIst,[45   55 75 90]); %get 4 ITI instances
% ITIst=prctile(ITIst,[25 45 55 75 90]); %get 5 ITI instances
iti0=[ITIst(randperm(length(ITIst))) ITIst(randperm(length(ITIst)))]; %randomize ITI

%% zerlege ITI in 2 phasen (startle in between)
iti1=iti0;%first period of ITI
iti2=iti1*0; %second period of ITI

%% add startle in ITI-phases
variafact=[.4 .6] ;%temporal factor of startle within ITI
ibalance=sign(rand(1,1)-.5)  ;%conditionzuordnung

for jj=[3 4]
    idv=find(iti0 ==ITIst(jj));
    if ibalance==1 %balance over conditons for ITIpair
        iti1new=  iti1(idv).*variafact;
    else
        iti1new=  iti1(idv).*fliplr(variafact);
    end
    iti2new=iti1(idv)-iti1new;
    iti1(idv)  =iti1new;
    iti2(idv)  =iti2new;

    ibalance=-ibalance; %revesere balance to balance 2nd ITIpair
end
tbstartle(:,7:8)=[iti1' iti2']; % ##### columns 7&8

%% add startleTIME after CS
% timestartle=[ .5  1 1.5 2  2.5 ];
timestartle=[ .5  1 1.5 2   ];
tbstartle(:,9)=[timestartle(randperm(length(timestartle))) timestartle(randperm(length(timestartle)))]; %randomize starleTIME

%% no US (GETRΔNK)
tbstartle(:,4)=0;

%% randomize TRL-order  (two orderiingCODES-->Mόnzwurf welcher genutzt wird)
% neworder=   [  1     6     7     2  8   5    9    10        4     3    ];
% tbstartle=tbstartle(neworder,:);


% 4randomisationMODI 

n2balance=4;
thisID=x.pbid_numeric;
% thisID=8
balanceID=rem(thisID,n2balance);
balanceID(balanceID==0)=n2balance;

if balanceID==1
    neworder=   [  3    5   4    1  8 7 2   6  ];
   tbstartle= tbstartle(neworder,:);
elseif balanceID==2
    neworder=   [  2    8  5    1   4 7 3  6  ];
   tbstartle=    tbstartle(neworder,:);
elseif balanceID==3
    neworder=   [    4 8  2  6  3  7   1  5      ];
     tbstartle=  tbstartle(neworder,:);
elseif balanceID==4
      neworder=   [    1  5    4 8  2  6  3  7      ];
     tbstartle=  tbstartle(neworder,:);
end

%% every 4 flipped: [x x x x]5,6,7,8,[x x x x],13,14,15,16
n2balance=8;
balanceID=rem(thisID,n2balance);
balanceID(balanceID==0)=n2balance;

if balanceID>=5  
    tbstartle=flipud(tbstartle);%fliporder
end


 


% 
% tbstartle2=tbstartle.*0;
% tbstartle2(  find(tbstartle(:,2)==1),:)=tbstartle(  find(tbstartle(:,2)==2),:);
% tbstartle2(  find(tbstartle(:,2)==2),:)=tbstartle(  find(tbstartle(:,2)==1),:);
% 
% %% BALANCE trialorder sheme OVER PBN
% if sign(rand(1)-.5) ==-1
%     tbstartle=tbstartle;
% end
% 
% tbstartle
 
% ==========================================
%% [2] prepare stuff
% ==========================================
preparewhitenoise(50,4);
setsoundvol( 4,sound.volStartle);
% ==========================================
%% [3] INTRO
% ==========================================
reminderStartle= double(imread([cuespath  'instr_startle.jpg'  ]));%STARTER-SCREEN
%         reminderStartle=imresize(reminderStartle,round([size(reminderStartle,1)/3 size(reminderStartle,2)/3]));
reminderStartle=reminderStartle./255;%scale to 0-1 range
reminderStartle(reminderStartle<0)=0;  reminderStartle(reminderStartle>1)=1;


%% [1]weiter mit [Space] Taste
clearpict(5);
% settextstyle('',25);

preparepict(  reminderStartle ,5, 0     ,0);
% drawpict(5);%

%   cgalign('r','c');
% cgtext(['nach links bzw. nach oben:'                       ],   0 , 0);
% cgpencol(0,0,1);
% cgalign('l','c');
% cgtext([' ' char(AD.navigationTXT{1})       ],   0 , 0);
% cgalign('c','c');

%  cgalign('l','c');
%  cgpencol(1,1,1);
%  cgtext([' ' char(AD.navigationTXT{1})       ],   0 , 0);

preparestring(['weiter mit [' AD.selectionTXT{1} ']'],               5,180, -250);

%    cgfont('courier',21) ;
%     cgtext('>> weiter mit        Maustaste',100 ,-160);
%     cgpencol(0,0,1);
%     cgtext('              linker          ',100 ,-160);

% cgsetsprite(5);
%  cgpencol(1,1,1);
% cgalign('r','c');
%   cgtext(['weiter mit  '       ],   0 , 0);
% cgpencol(0,0,1);
% cgalign('l','c');
%   cgtext([' ' char(AD.navigationTXT{1})       ],   0 , 0);
% drawpict(5);%



clearkeys;
drawpict(5);%INTRO-Display  ### MRmode
[ keyx mrk ] = p_waitkey( inf  ,[AD.esc AD.selection]);%loggin

% if g.isscanner==1;%In the scanner
%     drawpict(22);%GleichGehtsLos-Screen
%     %WAIT FOR SCANNER
%     [ keyx mrk ] = p_waitkey( inf, [key.esc{1,2} key.mr{1,2}  ] );% [ESC=52]or[scanner:name=K5;ID=32]
%     drawpict(20);%fixationScreen
%     t00=keyx(2);
%     trig=[trig;[  keyx(1)  keyx(2)-t00 t00]];%timestamp START registration: [trl_key_time]

% else%behavioural study
t00=drawpict(20);%-draw fixation & GET TIME
%     trig=[trig;[   0       t00-t00 t00]];    %timestamp START registration: [trl_key_time]
% end



if day==3
    reinstatementNumber=0; %just a dummy to separate from frmi-reinstatement
    reinstatement

    %        rating9(           'ratingfmri.xls',[ ],ima,x,0,AD ); %INTRO-SCREEN
    %reinstatement;

    x.rating1= rating9('ratingfmri.xls',[ ],ima,x,5,AD ); % rating-r2
    drawpict(20);%fix
end



%
initializestartle=1;

% wait(2000)
% playsound( 4 );% obligatorische ton ini
wait(3000)
%% INITIALIZE STARTLE
itistartle2=[1000 3000  1500  2500  ];
if initializestartle==1
    for i=1:4
        
        if i==1
            setsoundvol( 4,-1600);
        elseif i==2
            setsoundvol( 4,-1200);
        else
             setsoundvol( 4,sound.volStartle);
        end
        
        if isLPT==1 %LPT
            %outp(888,trl(6)); wait(tpulseLPT);  outp(888,0); %send marker(LEIPZIG)
            %putvalue(dio,trl(6)); wait(tpulseLPT);  putvalue(dio,0); %send marker
            dio=diosend(dio,[255   5 ]);
        end
        playsound( 4 );
        waitsound( 4 ); % Wait for sound to finish playing
        wait(itistartle2(i));
    end
end
wait(5000)


 

% ==========================================
%% [5] LOOP
% ==========================================
settextstyle('',35);


%______________________________________________
%   cut trials for DEMOTRIALS
%――――――――――――――――――――――――――――――――――――――――――――――
if demo==1
    tbstartle=tbstartle(1:demoNtrials,:) ;
end

x.tbstartle    =tbstartle;
tboutstartle   =x.tbstartle;
tbstartleRT    =[];
trialstime=tic;
for i=1: size(tbstartle,1)

    %timtrl=time;
    trl=tbstartle(i,:);
    %trigtrl=[];%monitorer
    clearkeys;%CLEAR KEYBOARDBUFFER
    %______________________________________________
    % show CUE   id:1001
    %――――――――――――――――――――――――――――――――――――――――――――――
    t0=time;
    tboutstartle(i,1)=t0-t00;
    clearpict(2);
    preparepict(imcue{trl(2)} , 2, loc(trl(3)),0);%prepare conditionRelated CUE
    %preparepict(imcue{trl(5)} , 2, loc(2),0);%prepare conditionRelated CUE
    preparestring('+',2);
    preparepuretone( trl(5), 100,1 );
    setsoundvol( 1,sound.volBeep);

    tIMG=drawpict(2);%CUE
    %     playsound( 1 );
    if isLPT==1 %LPT
        %outp(888,trl(6)); wait(tpulseLPT);  outp(888,0); %send marker(LEIPZIG)
        %putvalue(dio,trl(6)); wait(tpulseLPT);  putvalue(dio,0); %send marker
        dio=diosend(dio,[trl(6)   5 ]);
    end

    %______________________________________________
    % STARTLE
    % LPTcodes: 4+1=5 & 8+1=9
    %――――――――――――――――――――――――――――――――――――――――――――――

    % STARTLE IN ITI
    if trl(9)~=0
        wait(   t0+trl(:,9)*1000-time  );
        if isLPT==1 %LPT
            %outp(888,trl(6)); wait(tpulseLPT);  outp(888,0); %send marker(LEIPZIG)
            %putvalue(dio,trl(6)); wait(tpulseLPT);  putvalue(dio,0); %send marker
            dio=diosend(dio,[trl(6)+2   5 ]);
        end
        playsound( 4 );
        waitsound( 4 ); % Wait for sound to finish playing
    end


    %trigtrl=[trigtrl;[  1001 t0-t00 t0 ]];%TIMESTAMP: CUE x TIME
    wait(   t0+tim.pump-time  );% WAIT SELECTION-IMAGE

    % ______________________________________________
    % pumpe an
    % ――――――――――――――――――――――――――――――――――――――――――――――
    if trl(4)==1 %PUMPING
        
        %preparepuretone( 1550, 100, 3 );  playsound( 3);
        if isPump==1
            disp('pumpe an');
            [pid]=syringepump_ini(3,1);
        end
    else
        %preparepuretone( 300, 500, 3 );  playsound( 3);
    end

    readkeys;
    [mrk1 mrk2] = getkeydown;
    %______________________________________________
    % Abort experiment: ESC-key
    %――――――――――――――――――――――――――――――――――――――――――――――
    if~isempty(find(mrk1==AD.esc))%=ESC
        stop_cogent;
        %delete(findall(0,'tag','gui'));
        %error(' ....experiment terminated:[ESC] pressed....')  ;
        cprintf(-[0 .5 0],[ 'ΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌ\n'  ]);
        cprintf( [1,0.5,0],[ 'INFO '  ]);
        cprintf([0 0 1],[ '....session aborted:[ESC] pressed.... \n'  ]);
        cprintf(-[0 .5 0],[ 'ΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌΌ\n'  ]);
        return
    end

    %% get RESPONSEKEY
    if ~isempty(mrk1)
        %if trl(3)==1 & AD.navigation(1)
        tbstartleRT(i,:)=  [mrk2(1)-tIMG mrk1(1)];
    else
        tbstartleRT(i,:)=  [nan nan];
    end



    wait(   t0+tim.cue-time   );% WAIT SELECTION-IMAGE
    t1=drawpict(20);%CUE

    %=====ITI ========================================================================
    % WAIT ITI1
    %      wait(   t0+tim.cue+ round(trl(7)*1000) -time   ); %WAIT ITI
    wait(     round(trl(7)*1000)    ); %WAIT ITI

    % STARTLE IN ITI
    if trl(8)~=0
        if isLPT==1 %LPT
            %outp(888,trl(6)); wait(tpulseLPT);  outp(888,0); %send marker(LEIPZIG)
            %putvalue(dio,trl(6)); wait(tpulseLPT);  putvalue(dio,0); %send marker
            dio=diosend(dio,[255   5 ]);
        end
        playsound( 4 );
        waitsound( 4 ); % Wait for sound to finish playing
    end

    % WAIT ITI2
    %   wait(   t0+tim.cue+ round(trl(8)*1000) -time   ); %WAIT ITI
    wait(   round(trl(8)*1000)     ); %WAIT ITI

end%loop-end


%% AFTERWAEDS STARTLE
% wait(3000);
% if initializestartle==1
%     for i=1:4
%             if isLPT==1 %LPT
%                 %outp(888,trl(6)); wait(tpulseLPT);  outp(888,0); %send marker(LEIPZIG)
%                 %putvalue(dio,trl(6)); wait(tpulseLPT);  putvalue(dio,0); %send marker
%                 dio=diosend(dio,[255   5 ]);
%             end
%             playsound( 4 );
%             waitsound( 4 ); % Wait for sound to finish playing
%             wait(fliplr(itistartle2(i)));
%     end
% end


x.tbstartleRT=tbstartleRT;

%% STARTSTOPSESSIONMARKER
if isLPT==1 ;  dio=diosend(dio,[AD.LPTsessionStop   5 ]);  end



