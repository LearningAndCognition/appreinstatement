% clear, close all;
% clc;
% warning off;

function   rats=rating5(excelfile,display, ima,x,session,AD )

% excelfile='rating.xls';

%% get params
[b bb bbb]=xlsread(excelfile,2);
for i=2:size(bbb,1)
    try
        pa=str2num([bbb{i,2}]) ;
    catch
        pa= ([bbb{i,2}]) ;
    end
    va=bbb{i,1};
    eval(['para.' va '=pa;' ]);
end


%% get data
[a aa aaa]=xlsread(excelfile);
ihead=find(~cellfun(@isempty,regexpi(aa(:,1),'fieldnames')));
bb=aaa(ihead:end,2:end);

head=aaa(ihead,2:end);
data =aaa(ihead+1:end,2:end);

%% get session
% ses=1
isesL=find(~cellfun(@isempty,strfind(head,'session')));
ises =find([data{:,isesL}]==session);
dat1=data(ises,:);

%% get specific screens of session
iscrL=find(~cellfun(@isempty,strfind(head,'screenno')));
nscreens=unique([dat1{:,iscrL}]);

rats={};

%% show introframe
if session==0
    rat(display,para,ima, x,[],[],AD ); %SHOW INFO
    return
end



for iscr=1:length(nscreens)
    scr=   nscreens(iscr) ;%get screen
    iscr =find([dat1{:,iscrL}]==scr) ;%get row(s) of screen
    dat2=dat1(iscr,:);%get data of screen
    
    infox=rat(display,para,ima, x, head,dat2,AD ); %SHOW RATINGS
    rats=[rats; dat2 infox]  ; %get DATAOUT
end


% end
%
% RATING
%






    function  infox=rat(display,para,ima, x, head,dat2,AD )
        colbg=para.bgcolor;
        
        % % % fullscreenmode=1; %[0]small screen, [1] fullscreen
        %==========================================================================
        % text display: Note, BILD/~er will be replaced by Wort/Wφrter dynamically
        %==========================================================================
        if ~isempty(head) %trick ()
            
            %% get Question and POLs
            iaskL=find(~cellfun(@isempty,strfind(head,'question')));
            ask=dat2(:,iaskL:iaskL+2);
            
            %% get highlight (COLORIZE/HIGHLIGHT WORDS)
            % words2color={'angenehm' 'aufregend' 'beeinfluίt'};
            ihighL=find(~cellfun(@isempty,strfind(head,'highlight')));
            words2color=dat2(:,ihighL);
            
            %spacer for higlighting
            txtspac=2;
            for i=1:size(ask,1)
                dum= ask{i} ;
                fmt=repmat(' ', [ 1 length(words2color{i})   ]) ;
                wrd2color=words2color{i};
                if  isnan(wrd2color)
                    wrd2color='';
                end
                
                dum2=regexprep(ask{i,1}, wrd2color, fmt ) ;
                dum3=repmat(' ',[1 length(dum)  ])  ;
                ix=regexpi(dum2,fmt);
                dum3(ix+1:ix+length(words2color{i}) )=words2color{i};
                ask1(i,:)={dum2 dum3  };
            end
            
            %% get figure
            ifigL=find(~cellfun(@isempty,strfind(head,'imageno')));
            figno=dat2{1,ifigL};
            tbpics=ima.images;
            
            %% get element
            ielementL=find(~cellfun(@isempty,strfind(head,'element')));
            element=cell2mat(dat2(:,ielementL));
            
        end
        
        




%
%               START COGENT
%
wait(500);


cogentactive=iscogent;

if cogentactive==0;
    config_keyboard(100,1, 'nonexclusive' );% KEYBOARD
    config_mouse;
    fontsize=para.fstext;
    
    if ~isempty(display)
        config_display(display(1) ,display(2),    [para.bgcolor], [para.fgcolor], 'arial', fontsize,  10 ); % DELL
    else
        if x.screen.fullscreen==0
            config_display(0,1,                   [para.bgcolor], [para.fgcolor], 'arial', fontsize,  10 ); % DELL
        elseif x.screen.fullscreen==1
            %x.screen.screenmode=22 ;
            config_display(1,x.screen.screenmode, [para.bgcolor], [para.fgcolor], 'arial', fontsize,  10 ); % DELL
        end
    end
    %     cogent_screen=4  ; %screen
    %     if fullscreenmode==1
    %         config_display(1 , cogent_screen, [0 0 0], [1 1 1], 'arial', 25, 40 ); % EXP-PC
    %     else
    %         config_display(0 , cogent_screen, [0 0 0], [1 1 1], 'arial', 25, 40 ); % EXP-PC
    %     end
    start_cogent; %start cogent
end




map = getmousemap;
csd = cggetdata('GSD');
screensize=[ csd.ScreenWidth csd.ScreenHeight];

%
%             [1] DRAW INTRO-screen
%


if  isempty(head)
    
    try
        colbg=x.screen.colbg;
    catch
        colbg=para.bgcolor;
    end
    
    
    ask3= {'Bitte beantworten Sie die nδchsten Fragen'};
    cgflip(colbg);
    cgpencol(1,1,1);
    cgfont('Arial',35) ;
    %cgtext('Bitte geben Sie an,',0 ,  25);
    cgtext(ask3{1},0 ,  200);
    %         cgfont('',28) ;
    if strcmp(AD.device,'mouse')
        cgtext('Nutzen Sie dazu die Maus und die linke Maustaste',0 ,100);
        cgfont('courier',28) ;
        cgtext('>> weiter mit        Maustaste',100 , 120);
        cgpencol(0,0,1);
        cgtext('              linker          ',100 , 120);
    elseif strcmp(AD.device,'key')
        if length(AD.navigation)==2
            cgtext('Nutzen Sie dazu folgende Tasten: '                ,0 ,50);
            %                cgfont('courier',28) ;
            %                cgalign(l/c/r,t/c/b)
            
            cgalign('r','c');
            cgtext(['nach links bzw. nach oben:'                       ],   0 , 0);
            cgpencol(.2,.8,.2);
            cgalign('l','c');
            cgtext([' ' char(AD.navigationTXT{1})       ],   0 , 0);
            cgalign('c','c');
            
            cgpencol(1,1,1);
            cgalign('r','c');
            cgtext(['nach rechts bzw. nach unten:'                       ],   0 , -50);
            cgpencol(.2,.8,.2);
            cgalign('l','c');
            cgtext([' ' char(AD.navigationTXT{2})       ],   0 , -50);
            cgalign('c','c');
            cgpencol(1,1,1);
            
            cgpencol(1,1,1);
            cgalign('r','c');
            cgtext(['zum Einloggen:'                       ],   0 , -100);
            cgpencol(.2,.8,.2);
            cgalign('l','c');
            cgtext([' ' char(AD.selectionTXT{1})       ],   0 , -100);
            cgalign('c','c');
            cgpencol(1,1,1);
        else
            cgtext('Nutzen Sie dazu die Taste',0 ,100);
        end
        % cgfont('Arial',35) ;
        cgalign('r','c');
        cgtext(['>> weiter mit '           ],250 ,-250);
        cgalign('l','c');
        cgpencol(.2,.8,.2);
        cgtext([' '  char(AD.selectionTXT) ],250 ,-250);
        cgalign('c','c');
        
    end
    cgpencol(1,1,1);
    cgflip(colbg);%cgflip(0,0,0) ;
    if strcmp(AD.device,'mouse')
        clearmouse;
        readmouse ;
        waitmouse(4);
    elseif strcmp(AD.device,'key')
        waitkeydown( inf, [ AD.esc   AD.selection    ] )
    end

% infox={'into endet'}
% stop_cogent
return

end %showINTRO
%
%               set parameter
%


cgpenwid(3);
cgpencol(1,1,1);
% fontsizes=[16    21    24];
fontsizes=[20    21    28 35];
cgfont('Arial',fontsizes(1)) ;
blobcol=[0.2431    0.5176    0.7804];%[1 1 1];
% linlength=250;
% linhigh=round(0-screensize(2)/2+screensize(2)/10)   ;%-200;



%% IMAGE
npixresize=round(screensize(2)*.6);%300  %HIGHT OF IMAGE

lintik=5;  %ticklength
linlength0  = screensize(1)/7 ;%100; %VAS-Scalelength (e.g. 1/4 of SCRwidth)
linhigh=(-screensize(2)/4 )  ;  %yposition of first scale (e.g. lower 1/4 of SCRheight )
noScales=3  ;%number of scales
Scaldist=((screensize(2)/2+linhigh)-70)/(noScales-1 ) ;
hscaldist=-[0:noScales-1]*Scaldist; %UDpos.+additional value
txtdist     =20;
questdist   =40;
polTxtDist  =10;


% linlength=100; %length scale
% linhigh=-220; %UDpos. first line of scale
% hscaldist=-[1:3]*50; %UDpos.+additional value
% txtdist     =20;
% questdist   =20;
% polTxtDist =80;

%
%               START LOOP
%
loop=1; %
tic
% for j=1:2 %  pic vs word -order
if strcmp(AD.device,'mouse')==1
    clearmouse;  readmouse ; while getmouse(map.Button1) == 128 ; readmouse ;   end
end
cgflip(colbg);  %  cgflip(0,0,0) ;


infox={};
for i=1:1%ntrial%TRIAL=PICTURE=WORDS
    readkeys
    
    sccolor0   =[[para.scafocolor]     ;repmat(para.scafucolor,   [2 1]) ]; %SCALECOL
    txtHLcolor0=[[para.hlcolor   ]     ;repmat(para.scafucolor,   [2 1]) ];%TXTHIGLIGHTCOLOR
    
    
    if 0
        %[img mp]=imread([ tbpics{i,3} tbpics{i,4}]);
        %[img mp]=imread([ tbpics{i,1}  ]);
        [img mp]=imread([ ima.imagepath{i,1}  ]);
        try
            img=imresize(img,[ npixresize nan ]);
        catch
            img=p_imresize2(img,[ .5 nan ],'nearest');
        end
        
        img=permute(img,[2 1 3]);
        img2siz=[size(img,1) size(img,2)];
        img2=double(reshape(img,[prod(img2siz) 3]));
        img2=img2./255 ;
        width=round(img2siz*1);
        cgloadarray(1,img2siz(1),img2siz(2),img2,width(1),width(2)) ;
    end
    
    if figno~=0
        img= (tbpics{figno}).*255;
        img=permute(img,[2 1 3]);
        
        img2siz=[size(img,1) size(img,2)];
        img2=double(reshape(img,[prod(img2siz) 3]));
        img2=img2./255 ;
        width=round(img2siz*1);
        cgloadarray(1,img2siz(1),img2siz(2),img2,width(1),width(2)) ;
    else
        %             try
        %             cgloadarray(1,img2siz(1),img2siz(2),img2.*0,width(1),width(2)) ;
        %             end
        %             cgflip([]);cgflip(colbg);
        %             figno
    end
    
 
    
    for sc=1:size(ask1,1)%scale
        
       
        sccolor    =circshift(sccolor0   ,sc-1);
        txtHLcolor =circshift(txtHLcolor0,sc-1);
        
        if element(sc)==0 %scale element (scale,checkbox)
            linlength=linlength0;
        elseif element(sc)==1
            linlength=linlength0/8;
        end
        
        if strcmp(AD.device,'mouse')==1
            clearmouse;  readmouse ; while getmouse(map.Button1) == 128 ; readmouse ;   end
        elseif strcmp(AD.device,'key')==1
            clearkeys;
        end
        
        blobcol2=blobcol;
        xm=0;
        timex0=time;
        
        fastrun=[0 time]; %
        firstTimePresented=1;
        xmUr=xm;
        nloop=0;
       
        
        while loop==1
            nloop=nloop+1;
            cgalign('c','c');
            if figno~=0
                cgdrawsprite(1,0,100);
            end
            
            for o=1:size(ask1,1)
                
                
                
                %ELEMENT
                cgpencol(sccolor(o,1),sccolor(o,2),sccolor(o,3));
                
                if element(o)==0 %SCALE
                    
                    cgdraw(-linlength,  linhigh+hscaldist(o) ,linlength,  linhigh+hscaldist(o));%LTICK
                    cgdraw(-linlength,linhigh-lintik+hscaldist(o),-linlength,linhigh+lintik+hscaldist(o));%RTICK
                    cgdraw( linlength,linhigh-lintik+hscaldist(o), linlength,linhigh+lintik+hscaldist(o));
                    
                    %POLES
                    cgfont('Arial',para.fspoles) ;
                    cgalign('r','c')
                    cgtext(ask{o,2}     ,-linlength-polTxtDist ,linhigh+hscaldist(o)  );
                    cgalign('l','c')
                    cgtext(ask{o,3}     , linlength+polTxtDist ,linhigh+hscaldist(o)  );
                    
                    cgalign('c','c')
                    %QUESTION
                    cgfont('courier new',para.fsquestion) ;
                    %cgtext(ask{o,1},0 ,linhigh+hscaldist(o) +questdist);
                    cgtext(ask1{o,1},0 ,linhigh+hscaldist(o) +questdist);
                    cgpencol(txtHLcolor(o,1),txtHLcolor(o,2),txtHLcolor(o,3));
                    cgtext(ask1{o,2},0 ,linhigh+hscaldist(o) +questdist);
%                    guiElement=0;
                elseif element(o)==1 %CHECKBOX
                    
                    cgrect(-linlength ,linhigh-lintik+hscaldist(o)+5  ,20,20);
                    cgrect( linlength ,linhigh-lintik+hscaldist(o)+5  ,20,20);
                    
                    %POLES
                    cgfont('Arial',para.fspoles) ;
                    cgalign('r','c')
                    cgtext(ask{o,2}     ,-linlength-polTxtDist-20 ,linhigh+hscaldist(o)  );
                    cgalign('l','c')
                    cgtext(ask{o,3}     , linlength+polTxtDist+20 ,linhigh+hscaldist(o)  );
                    
                    cgalign('c','c')
                    %QUESTION
                    cgfont('courier new',para.fsquestion) ;
                    %cgtext(ask{o,1},0 ,linhigh+hscaldist(o) +questdist);
                    cgtext(ask1{o,1},0 ,linhigh+hscaldist(o) +questdist);
                    cgpencol(txtHLcolor(o,1),txtHLcolor(o,2),txtHLcolor(o,3));
                    cgtext(ask1{o,2},0 ,linhigh+hscaldist(o) +questdist);
%                    guiElement=1;
                elseif element(o)==2 %CHECKBOX-vertical
                    cgalign('c','c')
                    cgfont('courier new',para.fsquestion) ;
                    cgtext(ask1{o,1},0 ,linhigh+hscaldist(o) +questdist);
                     
                    %extract responses
                    ausw=ask{1,2};
                    sep=[0 strfind(ausw,char(10)) length(ausw)+1];
                    ausw2={};
                    for jj=1:length(sep)-1;
                        ausw2{jj,1}=ausw(sep(jj)+1:sep(jj+1)-1  );
                    end
                    
                    posquestion=linhigh+hscaldist(o) +questdist;
                    stpcheckb=50;
                    vecpos=posquestion-([1:size(ausw2,1)]*stpcheckb);
                    cgalign('l','c')
                    cgfont('Arial',para.fspoles) ;
                    for jj=1:size(ausw2,1)
                        cgrect(-75,vecpos(jj)  ,20,20);
                         
                       % cgfont('Arial',para.fspoles) ;
                        cgalign('l','c')
                        cgtext(ausw2{jj,1}  , -50 ,vecpos(jj) );
                    end
                    
                    startpos=[-75, mean(vecpos) ];
%                     if nloop==1
%                         xm0=startpos(2);
%                     end
%                                 cgflip(colbg);%cgflip(0,0,0) ;

                    
                    
                end
                
            end
           
            if strcmp(AD.device,'mouse')==1
                readmouse ;
                dx = getmouse(map.X)  ;
                dx=dx*2;%increase speed
                %                 cgtext(num2str(dx),0,0);
                if abs(xm+dx)<=(linlength)
                    xm =xm+dx;
                end
                
                if  getmouse(map.Button1) == 128
                    blobcol2=[1 1 1];
                    loop=0;
                    timex1=time;
                end %xm=0;
            elseif strcmp(AD.device,'key')==1 %keyboard
                xm0=xm;
                [xm fastrun AD loop]= keyVASresponse(xm, fastrun,AD,loop);
                
                if element(sc)==0 %SCALE
                    if xm<-linlength
                        xm=-linlength;
                    elseif xm> linlength
                        xm=linlength;
                    end
                elseif element(sc)==1  %CHECKBOX
                    %                    'okokok'
                    dfBox=(xm-xm0) ;
                    %                     posBoxes=[-linlength linlength ];
                    % %                    x00=posBoxes( find(abs(posBoxes-xm)==min(abs(posBoxes-xm))) );%genau in MItte
                    if dfBox~=0
                        xm= sign(dfBox)*linlength;
                    end
                    if xmUr==xm && loop==0;%damit icht einfach in mitte am anfang geloggt wird
                        loop=1;
                        clearkeys;
                    end
                 elseif element(sc)==2  %CHECKBOX-vertical  
                       
                     
                     if nloop==1
                        xm0=startpos(2);
                        xm =startpos(2);
                        xm2 =startpos(2);
                        te=0;
                        ticc=tic;
                        twait=.150;
                    elseif  xm~=xm2
                        
                        bvec=unique((sort([vecpos xm2])));
                        inext=find(bvec==xm2)-fastrun(1);
                        
                        if fastrun(1)~=0 &&  toc(ticc)>twait
                              %boundary
                            if inext<1 ; inext=1;end
                            if inext>length(bvec); inext=length(bvec);end
                            xm2=bvec(inext);
                            lastxm=xm2;
                            ticc=tic;
                        else
                          xm2=lastxm;  
                        end
                        xm=xm2;
                        xm0=xm;
                     end
                     
                     %have to responde
                     if loop==0 && xm2==startpos(2);
                         loop =1;
                         clearkeys;
                     end
                        
                        
                        
                        
                        
                        
                        
                        
                        
%                         if (xm<xm2) & toc(ticc)>twait
%                    
%                             if xm>=vecpos(end)
%                                 xm2=vecpos(min(find(vecpos<xm)));
%                             else
%                                 xm2=vecpos(end);
%                             end
%                             lastxm=xm2;
%                             ticc=tic;
%                             
%                         elseif (xm>xm2) & toc(ticc)>twait
%                             if xm<=vecpos(1)
%                             xm2=vecpos(max(find(vecpos>xm)));
%                             else
%                             xm2=vecpos(1);    
%                             end
% %                             timc=tic;
%                              lastxm=xm2;
%                              ticc=tic;
%                         else
%                           xm2=lastxm; 
%                         end
%                        xm=xm2;
%                        xm0=xm;
%                     end
                end
              
                if loop==0
                     timex1=time;
                end
                
                
            end%DEVICE
            
            
            if element(sc)~=2
                cgellipse( xm,    linhigh+hscaldist(sc) ,15,15, blobcol2-.2,'f');
                cgellipse( xm+3,  linhigh+2+hscaldist(sc) ,8,8, blobcol2,'f');
                cgellipse( xm+3,  linhigh+2+hscaldist(sc) ,3,3, [1 1 1],'f');
            else %verticalCHECKBOX
 
                 cgellipse(startpos(1)+10,xm2  ,15,15, blobcol2-.2,'f');
                cgellipse(startpos(1)+10, xm2 ,8,8, blobcol2,'f');
                cgellipse(startpos(1)+10, xm2 ,3,3, [1 1 1],'f'); 
                 timc=tic;
 
                
            end
            
            
            cgflip(colbg);%cgflip(0,0,0) ;
            
%             pause(5);
        end%loop
        loop=1;
        
        %WRITE TO TAB
        
        
        if element(sc)~=2
            value= (xm+linlength)*100/(2*linlength);
        else
            value=find(vecpos==xm2);
        end
        
        
        if figno~=0
            infox(end+1,:) ={ima.imagepath(figno) ima.imaglabel1(figno) ima.imaglabel2{figno} ...
               value ... %RATING
                timex1-timex0 ... %TIME ms
                };
        else
            infox(end+1,:) ={'empty' nan 'empty' ...
                value... %RATING
                timex1-timex0 ... %TIME ms
                };
        end
        %                  tbpics{i,sc+4}=(xm+linlength)*100/(2*linlength); %RATING
        %                 tbpics{i,sc+7}=timex1-timex0; %TIME ms
    end%scale
    
    %______________________________________________
    % Abort experiment: ESC-key
    %――――――――――――――――――――――――――――――――――――――――――――――
    readkeys;%read Keyboard
    [mrk1 mrk2] = getkeydown;%get all [Keys&timeStamp] since last clearkeys
    if~isempty(find(mrk1==AD.esc))%=ESC
        stop_cogent;
        error(' ....experiment terminated:[ESC] pressed....')  ;
    end
    
    
end%i IMAGE/word presentation
% end%j%QUESTION/scale
toc
cgflip(colbg);% cgflip(0,0,0) ;


% x.picrating{num}=tbpics;


if cogentactive==0
    %     stop_cogent;
end

return
%



function  [xm fastrun AD loop]= keyVASresponse(xm, fastrun,AD,loop);
    stp=1; 
    if fastrun(1)~=0      
        readkeys
        [ keyUp, t2 ] = getkeyup ;
        [ keyDown, t1 ] = getkeydown ;
       % '------------'
            if isempty(t1)
                if (time-fastrun(2))>200
                     xm=xm+fastrun(1)*4;
                elseif (time-fastrun(2))>100
                     xm=xm+fastrun(1)*2;
                else
                    xm=xm+fastrun(1);
                end
           end
       
        if ~isempty(keyUp)% fastRun ausstellen 
            fastrun=[0  time];
        end
        
        
        if ~isempty(keyDown)
           % 'DON ########'
            fastrun=[0  time];
            clearkeys
            readkeys
        end       
    else
        [ keyUp, t2 ] = getkeyup ;
        [ keyDown, t1 ] = getkeydown ;
        
%         stp2=0;
        if ~isempty(keyDown)
            if          keyDown(end)    == AD.navigation(1) ; stp2=-stp;%28; 
            elseif      keyDown(end)    == AD.navigation(2) ; stp2=+stp;%29; 
            elseif      keyDown(end)    ==  AD.selection(1);  loop=0; return ;%30;  SELECTION
            else      ; stp2=0;
            end
            
            
            
            if length(keyDown)==length(keyUp)
                return
            elseif length(keyDown)>length(keyUp)
                clearkeys
                readkeys
                %[ keyUp,   t2 ] = getkeyup ;
                fastrun=[stp2(1)   t1(1)]  ; %[STEPSIZE timerLastDOWN]
            else
               % '?????'
             end     
        else
           readkeys
        end
        
    end



