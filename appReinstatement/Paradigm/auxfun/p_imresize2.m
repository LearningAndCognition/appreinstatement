
function  [dum]= p_imresize2(X, fac_resiz, method   )
% function  [dum]= p_imresize2(X, fac_resiz, method   )
%   function  [dum]= p_imresize2(X, .85, 'cubic'   )


% fac_resiz=  .85 ;
% method='cubic'/nearest


% xx= 'C:\Dokumente und Einstellungen\skoch\Desktop\claudia_mail_28mar\Instruktion_oplern_26.03.13\Folie5.JPG'
% [X  ]=single ( imread(xx));


newSize=round([size(X,1) size(X,2) ].*fac_resiz )  ;



I = X; % original Bild
dimI = size(I);

[X Y] = meshgrid(1:dimI(2),1:dimI(1));
[Xnew Ynew] = meshgrid(linspace(1,dimI(2),newSize(2)),linspace(1,dimI(1),newSize(1)));
% Iresize = interp2(X,Y,I,Xnew,Ynew,'nearest'); % Farbwerte bleiben identisch

for i=1:size(I,3)
    %     dum(:,:,i) = interp2(X,Y,I(:,:,i),Xnew,Ynew,'nearest');  
    dum(:,:,i) = interp2(X,Y,I(:,:,i),Xnew,Ynew,  method);  
end
% dum=  (single(dum/max(dum(:)) ));
 
