
if 1

    config_keyboard(100,1, 'nonexclusive' );% KEYBOARD
    % config_results([resultdir name '.res']); %RESULTFILE
    config_display(0, 1, [0 0 0], [1 1 1], 'arial', 18, 40 ); % DISPLAY
%      config_sound; % 
      config_sound( 1, 16, 4000, 10 )
    
    start_cogent; %start cogent

    map=getkeymap;
    mapname=fieldnames(map)
    mapid=zeros(length(mapname),1);
    for i=1:length(mapname);
        mapid(i)=  getfield(map,mapname{i});
    end

end
% ----------------------------------------

clearpict(2)
preparestring('press any button for sound button ',2,0,-100);
preparestring( 'KEYCHECK ...hit [ESC] to quit ' ,2,0,200);
drawpict(2);
readkeys

drawpict(2);
% for i=1:5
ntr=1
co=0;
tic
b=toc;

soundvol=-1500;
step1=100
step2=500

preparewhitenoise(50,1);
% preparewhitenoise(40,2);
% preparewhitenoise(35,3);
preparepuretone( 400, 100,2 );
preparepuretone( 500, 100,3 );

tonmodus=1;
% multiplicator=.5;
step3=.1;

for i=1:10000

    %     if co==1
    clearkeys;
    %         clearpict(2);
    %         preparestring('press button',2,0,-100);
    %         drawpict(2);
    %         co=0;
    %     end


    readkeys;
    [ keyout, time, n ] = waitkeydown(inf);
    if ~isempty(keyout)%key pressed
        clearpict(2);
        
  
         setsoundvol( 1,soundvol );
         setsoundvol( 2,soundvol );
         setsoundvol( 3,soundvol );
%          preparewhitenoise2( 50,multiplicator, 4 )
         
        tIMG=drawpict(2);%CUE
        playsound( tonmodus );
        
        preparestring( 'SOUNDCHECK ...hit [ESC] to quit ' ,2,0,200);
         preparestring( ['current tonmodus:'  num2str(tonmodus)  ],2,0,160);
         preparestring( ['change tonmodus F1,F2,F3:'       ],2,0,120);
        preparestring( ['VOL : use arrows LR : small step :'  num2str(step1)  ],2,0,80);
        preparestring( ['VOL : use arrows UD : large step: '  num2str(step2)  ],2,0,40);
%         preparestring( ['multiplicator F4: '  num2str(multiplicator)  ],2,0,0);
        
        key=keyout(1);
        preparestring(['volume:  ' num2str(soundvol)   ] ,2,0,-40)
        c=toc;
%         preparestring(['temp.Diff.:  ' num2str(c-b)] ,2,0,-150);
        b=c;
        drawpict(2);
        %         wait(500);
        if strcmp(mapname{key} ,'Escape')
            stop_cogent;
            return;
        elseif strcmp(mapname{key} ,'Right')
            soundvol=soundvol+step1;
            if soundvol>-900; soundvol=-900; end
        elseif strcmp(mapname{key} ,'Left')
            soundvol=soundvol-step1;
            if soundvol<-10000; soundvol=-10000; end
        elseif strcmp(mapname{key} ,'Up')
            soundvol=soundvol+step2;
            if soundvol>-1000; soundvol=-1000; end
        elseif strcmp(mapname{key} ,'Down')
            soundvol=soundvol-step2;
            if soundvol<-10000; soundvol=-10000; end
        elseif strcmp(mapname{key} ,'F1')
            tonmodus=1;
        elseif strcmp(mapname{key} ,'F2')
            tonmodus=2;
        elseif strcmp(mapname{key} ,'F3')
            tonmodus=3;
%           elseif strcmp(mapname{key} ,'F4')
%             tonmodus=4;  
%         elseif strcmp(mapname{key} ,'N')
%             multiplicator=multiplicator-step3;
%            if multiplicator<0 ; multiplicator=0; end
%         elseif strcmp(mapname{key} ,'M')
%             multiplicator=multiplicator+step3;
            
        end



        ntr=ntr+1;

    else


    end



end

stop_cogent;