function [pid]=syringepump_ini(do,varargin)


% [pid]=syringepump_ini(1,'npumps',1, 'DIA',28,'RAT',110,'VOL',.5)
% [pid]=syringepump_ini(1,'npumps',4, 'DIA',28,'RAT',110,'VOL',.5)
% [pid]=syringepump_ini(1,            'DIA',28,'RAT',110,'VOL',.5)
% [pid]=syringepump_ini(0);%remove/deinstall pumps (do this after EXP)
% [pid]=syringepump_ini(1);%setup pumps (do this first)
% [pid]=syringepump_ini(2);%just a tester
% [pid]=syringepump_ini(3,1);%run pump1
%skoch2014

warning off;
%
%
if do==0,
    pid=instrfind('type','serial','port','COM1');
    if ~isempty(pid)
        fclose(pid);
        pid=[];
%         try; clear pid;end
    end

elseif do==1

% [pid]=syringepump_ini(1,'DIA',28,'RAT',110,'VOL',0.5)
  if mod(nargin,2)~=1;  error('additional fields must come in pairs'); end


  
    pid=instrfind('type','serial','port','COM1');
    if ~isempty(pid)
        fclose(pid);
        delete(pid);
        try; clear pid;end
    end
    pid = serial('COM1',...%::: Verbindung herstellen :::%
        'BaudRate', 9600,...
        'Parity', 'none',...
        'DataBits', 8,...
        'StopBits', 1,...
        'Terminator','CR',...
        'Timeout', .1);
    fopen(pid);            % open pumps
    %     disp ('COM-Port opened successfully');
    %     d = fscanf(pid); % Empfangen bis ich den Port selber schliesse
    %     disp(d)


    query(pid,'*IDN');%this stops the pumps display flashing

    %-------[DEFINITION: syringe pump NE-1010 ]---------------------------------
    %  100er spritze:  34mm, rate: 160
    % omnifix braun/B  28mm sprite, maxrate: 110

    query(pid,['*DIA 28']) ;%inner diameter of syringe
    pause(0.1);% RATE (pumpingVelocity/timex): number & unit ..=velocity
    query(pid,['*RAT 110' 'MM']) ;  %..[MM, UH, MH]..[mL/min muL/Hour mL/Hour]
    pause(0.1);
    query(pid,['*VOL 0.5'])   ;% VOLUME pumped ..here 10ml
    pause(0.1);
    query(pid,'*DIR inf');%pumping direction [ inf | Wdr | rev ]; infuse/withdraw/reverse

    npumps=1;
    if isempty(varargin)
        addparas={};
    else
         addparas=reshape(varargin,[2 length(varargin)/2]);

        
        %get No of pumps
        ipump=find(~cellfun('isempty',regexpi(addparas(1,:),'npumps')));
        if ~isempty(ipump)
            npumps=addparas{2,ipump};
            addparas(:,ipump)=[];%delete Npumps-variable
        end
        
       
        for i=1:size(addparas,2)
              pause(0.1);
              query(pid,['*' addparas{1,i} ' ' num2str(addparas{2,i})   ]) ;
        end
    end
    
    
    if 0
        query(pid,[num2str(1)  'RUN'])
    end
    % -----------------only testers--------------------------
    %TEST pumps
    timx=linspace(0.1, .4 ,npumps) ;%beepingtime
    tag={};
    for i=1:npumps %3 pumps
        tag{i}= query(pid,[num2str(i-1) 'BUZ 1' ]);
        pause(timx(i));
        query(pid,[num2str(i-1) 'BUZ 0' ]);
        pause(.3);
    end
    pumpNerror=find(cellfun('isempty',tag)~=0);
%     if ~isempty(pumpNerror);%one or more pumps not working
%         fprintf('\n PUMP %d is not working\n', pumpNerror);
%         try
%             SV = actxserver('SAPI.SpVoice');
%             invoke(SV,'Speak',[' PUMP ' [regexprep(deblank(num2str(pumpNerror)),'  ',' and ')] ' is not working'  ]);
%             delete(SV); clear SV;
%             pause(0.2);
%         end
%     else
%         SV = actxserver('SAPI.SpVoice');
%         invoke(SV,'Speak',['good morning sir, your pumps are ready'  ]);
%         delete(SV); clear SV ;
%         pause(0.2);
%     end


    % -----------------------------make a test-----
elseif do==2
    pid=instrfind('type','serial','port','COM1');
    for i=1:4*3
        query(pid,[num2str(mod(i,4))  'RUN']);%run pump with adr 0
         % pause(rand(1))
          pause(.2);
    end
elseif do==3
    pid=instrfind('type','serial','port','COM1');    
   query(pid,[num2str(varargin{1}-1)  'RUN']); %run specific pump

end

 
%

% warning on;

% %
% % query(pid,'*RUN'); %run all pumps
% % pause(0.1);
% % query(pid,'0RUN');%run pump with adr 0
% % pause(0.1);
% % query(pid,'1RUN');%run pump with adr 1
% %
% % for i=1:3*10
% %    query(pid,[num2str(mod(i,3))  'RUN']);%run pump with adr 0
% % %    pause(rand(1))
% %  pause(.2)
% % end




% %
% % query(pid,'*STP')
% %
% % fclose(pid);




% % % % if strcmp(do,'initialize')
% % % % g=instrfind('type','serial','port','COM1');%close open connection
% % % %     if ~isempty(g)
% % % %         fclose(g);
% % % %         delete(g);
% % % %     end
% % % % %::: Verbindung herstellen :::%
% % % %     pid = serial('COM1',...
% % % %            'BaudRate', 9600,...
% % % %            'Parity', 'none',...
% % % %            'DataBits', 8,...
% % % %            'StopBits', 1,...
% % % %            'Terminator','CR',...
% % % %            'Timeout', .1)
% % % %
% % % %
% % % %     fopen(pid);            % Gerδt φffnen
% % % % %     disp ('COM-Port opened successfully');
% % % % %     d = fscanf(pid); % Empfangen bis ich den Port selber schliesse
% % % % %     disp(d)
% % % %
% % % %
% % % % query(pid,'*IDN')
% % % %
% % % % %-------[DEFINITION: syringe pump NE-1010 ]---------------------------------
% % % % %  100er spritze: 34mm, rate: 160
% % % % % 21mm sprite, maxrate: 60
% % % %
% % % % query(pid,['*DIA 21']) ;%inner diameter of syringe
% % % % pause(0.1);% RATE (pumpingVelocity/timex): number & unit ..
% % % % query(pid,['*RAT 60' 'MM']) ;  %..[MM, UH, MH]..[mL/min muL/Hour mL/Hour]
% % % % pause(0.1);
% % % % query(pid,['*VOL 0.5'])   ;% VOLUME pumped ..here 10ml
% % % % pause(0.1);
% % % %
% % % %
% % % % query(pid,'*RUN'); %start pumping ..stops automatically
% % % %
% % % %
% % % %
% % % % query(pid,'*STP')
% % % %
% % % % fclose(pid);





