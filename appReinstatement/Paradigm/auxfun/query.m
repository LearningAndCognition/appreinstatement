% if query function not available: older version
% function query(id,str)


function out=query(obj,cmd)
%  fprintf(id,str);

 wformat = '%s\n';
 [formattedCmd, errmsg] = sprintf(wformat, cmd);
 out = query(igetfield(obj, 'jobject'), formattedCmd);