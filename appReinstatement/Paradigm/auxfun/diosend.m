function d=diosend(d,varargin)
% function d=diosend(d,varargin) %send marker to DIO
% d=diosend('ini1');%using DIGITALIO /DAQ-TB needed
% d=diosend('ini2'); %using outp.m (5files)
% d=diosend(d,[10 5  ]) ;%send marker 10 for 5ms  
% d=diosend(d,[10 5 255 5 ]) ;%send marker 10 for 5ms and mrk 255 for 5ms

 aa=tic;

% d=varargin{1};
if strcmp(d,'ini1')
   d.dio=digitalio('parallel',1);
   d.ini=1;
   d.info='using [digitalio/DAQ]';
   addline(d.dio,0:7,'out');
elseif strcmp(d,'ini2')
    config_io;
    d.ini=2;
    d.info='using [config_io]';
    
elseif nargin>1
     if d.ini==1
         for i=1:2:length(varargin{1})
             putvalue(d.dio, varargin{1}(i)   ); wait(  varargin{1}(i+1) );   ; %send marker
         end
         putvalue(d.dio,0);
     elseif d.ini==2
         
         for i=1:2:length(varargin{1})
             outp(888, varargin{1}(i)   ); wait(  varargin{1}(i+1) );   ; %send marker
         end
          outp(888, 0  );
     end
   
    
end

d.diotime=toc(aa)*1000; %time markers needed

 

