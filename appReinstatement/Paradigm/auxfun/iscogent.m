function is=iscogent
% function is=iscogent
%checks whether cogent is running:  [is]=true/false
try
    [ comment s]=evalc('cggetdata(''GSD'')');
    is=isstruct(s);

catch
     [ comment   ]=evalc('cggetdata(''GSD'')');
      %  is =exist( s ,'var');
      is=0;
end