function [ keyx allmat ] = p_waitkey( duration, keyin )
% WAITKEY Read and log previous keypresses.
%
% Wait for a specific key to be pressed after the call to waitkey.
% duration..im ms
% keyin..monitored keys to wait for
% keyx: key to wait for and timestamp
% allmat: all other keys and timeszamps (inclusively keyx)
% example:
% [ keyx allmat ] = p_waitkey( 2000, [ 5  18  4  6] )
global cogent;

event=128;%keydown
t0 = time;
keyx = [];
t = [];
allmat=[]; 

% Handle any pending key presses from before waitkey call
readkeys;
% logkeys;


while isempty(keyx)  &  time-t0 < duration
    
    readkeys;
%     logkeys;
    
        indexall = find( cogent.keyboard.value == event ) ;
        index = find( cogent.keyboard.value == event & ismember(cogent.keyboard.id,keyin) );
   
    
%     keyout = cogent.keyboard.id( index );
%     t      = cogent.keyboard.time( index ); 
   keyx=[cogent.keyboard.id( index ) cogent.keyboard.time( index )];
   allmat=[allmat ; [cogent.keyboard.id( indexall ) cogent.keyboard.time( indexall )]];
    
%     pause(0.001)
    
end




