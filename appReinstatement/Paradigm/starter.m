%%  3DAY experiment
%% potential adjustments/checks:
%  - adjust screenmode,
%  - parallel and serial ports (currently disabled..see  PARAMETER: PORTS & SCREEN)
%     ..LPT is only needed if markers should be send to another device (LPT)
%     ..SERIAL PORT: only needed when using a syringe pump
%  - response keys (use: p_keychecker)
%
%% current KEYBOARD setting: 
%  outside scanner (sessions: 1,2,3)
%    # left/right arrow keys to ...
%        -select left/right image
%        -move dot in VAS
%    # SPACE: to accept VAS, clear intro-screen
%    # RETURN: start subtask by investigator
%  in scanner (session: 4)
%   keys 3 & 4 (from optical response keys) == keys 3&4 from keyboard
%        -select left/right image
%        -move dot in VAS 
%    # key 1 (from optical response keys) == key 1 from keyboard 
%         -to accept VAS or clear intro-screen
%    # key 5 (from optical response keys) == key 5 from keyboard
%          -MRI-EPI trigger, 
%            -when running session-4 outside MRI-environment you have to
%             hit key-5 after the intro-screen to terminate the 'wait for 
%            skanner marker'-loop  
%             
%
%_________________________________________________________________________


clear;close all;clc

%E
%%   add COGENT path (optional)
%E
if 1
    if isempty(which('start_cogent.m'))
        addpath(genpath(fullfile(fileparts(pwd),'Cogent2000v132')));
    end
end

%% ADD ADITIONAL FUNCTIONS/SCRIPTS
addpath(fullfile(pwd,'auxfun'));


%E
%%   PARAMETER: PORTS & SCREEN
%E

isLPT                    = 0   ;% PARALLELPORT: [0,1]no/yes LPT-port marker send e.g. to EDA  LEIPZIG
isPump                   = 0   ;% syringePump via serial port
screen.fullscreen        = 1   ;%SCREENSIZE:   [0,1]no/yes FULLSCREEB
screen.screenmode        = 4     ;% [5]tested with SARGCE,  [4] used for EXPERIMENT] ;

%---------------------
demo          =1; % [0] real EXPlength,[1]demo(shortVersion)
demoNtrials   =4; % only used if "demo==1"

%E
%%   PARAMETER: AUDIO
%E
%soundVolume [0..1] leise-laut
sound.volBeep    =-2500;
sound.volStartle =-900;
%E
%%    userinput, pbID+day
%E
prompt = {'pbcode (zb: r01 ) mit "0"! ',...
    ['SessionNr' char(10) ...
    'day1...........................: 1' char(10)...
    'day2...........................: 2' char(10) ...
    'day3-outside scanner: 3 ' char(10) ...
    'day3-in scanner.........: 4 ' ]};
dlg_title = 'kitchenFMRI';
num_lines = 1;
def = {'',''};
def = {'r0',''};
answer = inputdlg(prompt,dlg_title,num_lines,def);

pbid        = answer{1};
pbid_numeric=str2num(regexprep(pbid,'[A-Za-z]',''));
day         = str2num(answer{2});

drawnow
close all
pause(1)

%% make database: on firstday only
if day==1
    x='';
    [xdum errors]=createTrialtableFMRI( ...
        pbid_numeric,pbid,x);
end




%______________________________________________
%%  LOAD DAYWISE DATA
%______________________________________________
datafile=fullfile(pwd,'data', [pbid '_day' num2str(day) ]  ) ;
load( datafile );

%E
%%   PARAMETER: KEYS
%E
% % key.esc         =p_getkeycodes({'escape'});%escape-key
% % key.continue    =p_getkeycodes({'space'});%SPACE-key
% % key.mr          =p_getkeycodes({'K5'});%MR-trigger
%=====================
% RESPONSE KEYS
%=====================
%
%%  Ebehavioral E
AD.device       ='key'  ;%'mouse'
AD.esc          =52;
AD.scanner      =32    ;%this is key-5  ; echecked via:  p_getkeycodes({'K5'})
AD.instructor   =59    ;%return=[59]; enter=[90]          ; echecked via:  p_getkeycodes({'return'})
x.AD=AD;
%% SCANNERE
if day==4 %day1 & day2  BEHAVIORALE
    AD.navigation   =[30 31];%[tasten 3&4]=[30,31]
    AD.navigationTXT={'linke Taste' 'rechte Taste'};
    AD.selection    =[28];%[tasten 1]=[28]
    AD.selectionTXT ={'linke Taste linke Hand'};
else % OUTSIDE SCANNER
    usekeypad=0; % USING EXTENDED KEYPAD (used in STUDY)
    if usekeypad==1
        AD.navigation   =[77 78]; % KEYS FROM KEYPAD
        AD.navigationTXT={'linke Taste' 'rechte Taste'};
    else
        AD.navigation   =[97 98]; %ARROWKEYS LEFT/RIGHT
        AD.navigationTXT={'linke Taste' 'rechte Taste'};
    end
    
    AD.selection    =71;%space=[71]
    AD.selectionTXT ={'Leertaste'};
    
end

%E
%%   PARAMETER: LPT.MARKER
%E
AD.LPTsessionStart=16;
AD.LPTsessionStop =32;

%E
%%   PARAMETER: TIMING
%E
%     Timing zunδchst CS fE 4 s zeigen. 1 s vor CS offset erfolgt in
%     verstδrkten CS+ FlEsigkeitsgabe Eer Pumpen. ITI im Mittel 6s, Minimum
%     3.5s.
%     Das fErt bei insgesamt 120 Trial zu einer Dauer von 20 min:
%     ((3+1)+6)*60*2/60
%
% ITI: ITI time [min/mean/max]:  3.5018    5.9927   12.0000
%                                %[min(tb(:,7)) mean(tb(:,7)) max(tb(:,7))]
tim.cue         =4000;        %%time[ms]
tim.pump        =tim.cue-1000;%%time[ms]
tim.mrtimepre   =1000; %MR-baseline before
tim.mrtimepost  =10000;%10000;%MR-baseline after
x.tim=tim;       %save4documentation



%______________________________________________
%% INI PUMP
%――――――――――――――――――――――――――――――――――――――――――――――
if isPump==1
    % [pid]=syringepump_ini(1,'npumps',1, 'DIA',28,'RAT',110,'VOL',.5);
    [pid]=syringepump_ini(1,'npumps',1, 'DIA',28,'RAT',110,'VOL',1);
end
%______________________________________________
%% INI  LTP (BERLIN/LEIPZIG)
%――――――――――――――――――――――――――――――――――――――――――――――
if isLPT==1 %MPI-LPTPORT_INITIALIZE
    dio=diosend('ini2'); %using outp.m (5files)
end
%______________________________________________
%%  LOAD IMAGES
%――――――――――――――――――――――――――――――――――――――――――――――
cuespath=[pwd '\stimuli\'];
cues=dir([cuespath  x.stimulustoken  ]);%****LOAD CUES
imcue={};
for i=1:length(cues)
    dum=single (imread([pwd '\stimuli\' cues(i).name ]));
    dum=p_imresize2(double(dum) ,[.4],'spline');
    dum=dum./255;%scale to 0-1 range
    dum(dum<0)=0;  dum(dum>1)=1;
    imcue{i,1}=dum;
    cuesfiles{i,1}=fullfile(cuespath,cues(i).name);
end
if 1
    reminder= double(imread([cuespath  x.stimulusinstr  ]));%STARTER-SCREEN
    reminder=reminder./255;%scale to 0-1 range
    reminder(reminder<0)=0;  reminder(reminder>1)=1;
end
x.cuesfiles=cuesfiles; %add cuenames
loc   =[-170 170]; %location of images l/r  [-150 150]



%______________________________________________
%% COGENT-INI + START (screen/keyboard-config)
%――――――――――――――――――――――――――――――――――――――――――――――
fontsize          =25;
resultdir         =[pwd '\data\']; %destination:resultfile

screen.colbg      =[0 0 0];%[.5 .5 .5];%background-color
screen.colfg      =[1   1  1];%foreground-color
screen.nbuffer    =40        ;%N of buffers

screen.screenresall={1  640 480 ; 2  800 600 ; 3  1024 768 ; 4  1152 864 ; 5  1280 1024 ; 6  1600 1200 };%!!you cannot change this!!


if 1
    config_mouse;
    if exist( fullfile(pwd,'data'))~=7; eval(['!mkdir data']); end % make 'data' folder if dosn't exist
    config_keyboard(100,1 );% KEYBOARD
    if screen.fullscreen==0
        config_display(0,1, [screen.colbg], [screen.colfg], 'arial', fontsize, screen.nbuffer ); % DELL
    else
        config_display(1,screen.screenmode, [screen.colbg], [screen.colfg], 'arial', fontsize, screen.nbuffer ); % DELL
    end
    screen.screenres = screen.screenresall(screen.screenmode,:);
    %     pause(1)
    config_sound; % mono, 16 bits per sample, 11025 samples per sec, 100 buffers
end


%     config_keyboard(100,1, 'nonexclusive' );% KEYBOARD
% %     config_results([resultdir name '.res']); %RESULTFILE
% %     config_display(1, 4, [0 0 0], [1 1 1], 'arial', 25, 40 ); % DELL
% %     config_display(1, 2, [0 0 0], [1 1 1], 'arial', 25, 40 ); % IBM-r40
%  config_display(1, 5, [0 0 0], [1 1 1], 'arial', 25, 40 ); % EXP-PC (SARGE)
%  %  config_display(0, 1, [0 0 0], [1 1 1], 'arial', 25, 40 ); % dell-NIRX
%     start_cogent; %start cogent
%


%E
%  START COGENT
%E
start_cogent; %start cogent


%
%______________________________________________
% code instruction and fixed buffers
%――――――――――――――――――――――――――――――――――――――――――――――
%blackScreen
preparestring( ' ', 19);

%%[0]FIXcross
settextstyle('',35);
preparestring( '+', 20);
settextstyle('',fontsize);

%%[1]instruction
settextstyle('',10);
preparepict(  reminder ,21, 0     ,0);
% preparestring('weiter mit [Space] Taste',               21,150, -220);
% setforecolour(.8,.16,0);
% settextstyle('',15);
% preparestring('ADMIN:<<hit "Enter">>' ,  21, 0, -250);
% setforecolour(1,1,1)



%%[2] ..gleich gehts los..
settextstyle('',35);
preparestring( '..gleich gehts los..'    ,22);
settextstyle('',35);


%______________________________________________
%%  RATING preparation
%――――――――――――――――――――――――――――――――――――――――――――――
ima=[];
ima.images    =imcue ;
ima.imagepath =x.cuesfiles;
%% ima.imaglabel1 (bool for outcome)
ima.imaglabel1=zeros(1,length(ima.imagepath));
ima.imaglabel1(x.figWithoutcome) =1;
%% ima.imaglabel2  (label for outcome)
ima.imaglabel2{find(ima.imaglabel1 ==1)}='Outcome' ;
ima.imaglabel2{find(ima.imaglabel1 ==0)}='noOutcome';

%% update screen
x.screen=screen;

%% FIRST SCREEN
clearkeys;
drawpict(19);%blackScreen
% drawpict(22);%INTRO-Display  ### MRmode
% [ keyx mrk ] = p_waitkey( inf  ,AD.instructor );%Eait for 'SPACE'


% countdown(3000,6 );

%
if day==1  % DAY-1
    %
    
    % ===============================
    % *** DAY1 ***
    % ===============================
    
    g.isscanner=0;
    
    
    rating9(           'ratingfmri.xls',[ ],ima,x,0,AD ); %INTRO-SCREEN
    x.rating1= rating9('ratingfmri.xls',[ ],ima,x,1,AD ); %rating-r1
    
    experiment;
    x.rating2= rating9('ratingfmri.xls',[ ],ima,x,2,AD ); % rating-r2
    
    
    startle ;
    %x.puzzle=puzzle_kb([],x.pbid_numeric ,[3 3],ima,2,AD);   % puzzle
    
    %
elseif day==2  % DAY-2
    %
    
    g.isscanner=0;
    
    rating9(           'ratingfmri.xls',[ ],ima,x,0,AD ); %INTRO-SCREEN
    x.rating1= rating9('ratingfmri.xls',[ ],ima,x,3,AD ); %rating-r1
    
    experiment;
    x.rating2= rating9('ratingfmri.xls',[ ],ima,x,4,AD ); % rating-r2
    startle % startle
    
    
    %
elseif day==3  % DAY-3 :outside SCANNER
    %
    
    g.isscanner=0;
    
    %     rating9(           'ratingfmri.xls',[ ],ima,x,0,AD ); %INTRO-SCREEN
    %reinstatement;
    %     x.rating1= rating9('ratingfmri.xls',[ ],ima,x,5,AD ); % rating-r2
    
    startle % startle
    
    %
elseif day==4  % DAY-3 :IN SCANNER
    %
    g.isscanner=1;
    experiment;
    x.rating2= rating9('ratingfmri.xls',[ ],ima,x,6,AD ); % rating-r2
    
end



%=========================================================================
%                       END
%=========================================================================
disp('___ENDE___');
stop_cogent;



%E
%%   additional information
%E
x.isscanner =g.isscanner;
x.completed =1; %this session is completed

%E
%%   SAVE DATA
%E
outfile=[datafile '_done'];
save(outfile ,'x') ;%save data and move to 'completed'-folder

%% backup
bkfolder=fullfile(pwd,'data','backup');
mkdir(bkfolder);
bkfile=[pbid '_day' num2str(day) '__' datestr(now,'dd-mm-yy#_HH_MM_SS') ]    ;
save( fullfile(bkfolder,bkfile)  ,'x');



