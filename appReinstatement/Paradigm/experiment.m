
%% STARTSTOPSESSIONMARKER
if isLPT==1 ;  dio=diosend(dio,[AD.LPTsessionStart   5 ]);  end

%______________________________________________
%  GET ABSOLUTE TIME //wait for SCANNER pulse
%
clearkeys;
drawpict(21);%INTRO-Display  ### MRmode
[ keyx mrk ] = p_waitkey( inf  ,AD.instructor );%厀ait for 'SPACE'



if g.isscanner==1;%In the scanner
    trig  =[];%all trigger matrix
    
%     setforecolour(.8,.16,0);
%     preparestring('ADMIN:<<SCANNER-fMRI-SEQUENCE nun starten>>' ,  22, 0, -100);
%     setforecolour(1,1,1)
%     drawpict(22);%GleichGehtsLos-Screen
     drawpict(20);%
    
    %WAIT FOR SCANNER
    [ keyx mrk ] = p_waitkey( inf, [AD.esc AD.scanner  ] );% [ESC=52]or[scanner:name=K5;ID=32]
    drawpict(20);%fixationScreen
    t00=keyx(2);
    trig=[trig;[  keyx(1)  keyx(2)-t00 keyx(2)-t00]];%timestamp [key dTime absTime]

else%behavioural study
    trig  =[];%all trigger matrix
    t00=drawpict(20);%-draw fixation & GET TIME
    trig=[trig;[   0       t00-t00 t00]];    %timestamp START registration: [trl_key_time]
end

%WAIT SOME SECONDS=BASELINE
clearkeys;

if day==4 %fmri-reinstatement
    reinstatementNumber=1;
    reinstatement
end
wait(tim.mrtimepre);% MR-baseline





%______________________________________________
%   cut trials for DEMOTRIALS
%
if demo==1
    x.tb=x.tb(1:demoNtrials,:) ;
end


 
% ==============================================================================
%    LOOP over TRIALS
% ==============================================================================
settextstyle('',35);
tb   =x.tb;
tAllTrials=tic;
tbRT=[];
for i=1: size(tb,1)

    
    if day==4 %fmri-reinstatement after hlof of the trials
        if i==round(size(tb,1)/2)+1 
        reinstatementNumber=2;
        reinstatement
        end
    end
    
    
    trl=tb(i,:);
    clearkeys;%CLEAR KEYBOARDBUFFER
    %______________________________________________
    % show CUE   id:1001
    %
    t0=time;
    clearpict(2);
    preparepict(imcue{trl(2)} , 2, loc(trl(3)),0);%prepare conditionRelated CUE
    preparestring('+',2);
    %preparepuretone2( trl(5), 100,soundfactor, 1 );
    preparepuretone( trl(5), 100,1 );
    setsoundvol( 1,sound.volBeep);
    
    tIMG=drawpict(2);%CUE
    playsound( 1 );

    if isLPT==1 %LPT
        dio=diosend(dio,[trl(6)   5 ]);
    end
    trig=[trig;[  1001 tIMG-t00 tIMG-t0 ]];

    
    
    %trigtrl=[trigtrl;[  1001 t0-t00 t0 ]];%TIMESTAMP: CUE x TIME
    wait(   t0+tim.pump-time  );% WAIT SELECTION-IMAGE

    %% get RESPONSEKEY
    readkeys;
    [mrk1 mrk2] = getkeydown;
    
    if ~isempty(mrk1)
        %if trl(3)==1 & AD.navigation(1)
        tbRT(i,:)=  [mrk2(1)-tIMG mrk1(1)];
    else
        tbRT(i,:)=  [nan nan];
    end
    
    % ______________________________________________
    % pumpe an
    % 
    if trl(4)==1 %PUMPING
        if isPump==1
            disp('pumpe an');
            %preparepuretone( 1550, 100, 3 );  playsound( 3);
            [pid]=syringepump_ini(3,1);
        end
    else
        %preparepuretone( 300, 500, 3 );  playsound( 3);
    end

 
    %______________________________________________
    % Abort experiment: ESC-key
    %
    if ~isempty(find(mrk1==AD.esc))%=ESC
        stop_cogent;
        %delete(findall(0,'tag','gui'));
        %error(' ....experiment terminated:[ESC] pressed....')  ;
        cprintf(-[0 .5 0],[ '技技技技技技技技技技技技技技技技技技技技技技技技技技技技技技\n'  ]);
        cprintf( [1,0.5,0],[ '旾NFO� '  ]);
        cprintf([0 0 1],[ '....session aborted:[ESC] pressed.... \n'  ]);
        cprintf(-[0 .5 0],[ '技技技技技技技技技技技技技技技技技技技技技技技技技技技技技技\n'  ]);
        return
    end

    wait(   t0+tim.cue-time   );% WAIT SELECTION-IMAGE
    t1=drawpict(20);%CUE
    wait(   t0+tim.cue+ round(trl(7)*1000) -time   ); %WAIT ITI

end%EXPERIMENT-end

x.trig =trig; %timing FMRI
x.tbRT =tbRT; %RESPONSE 
x.tAllTrials     =toc(tAllTrials);
x.tAllTrialsMin  =x.tAllTrials/60;
x.timeEXP=t00-time;
wait(tim.mrtimepost);%  MR-baseline

%% STARTSTOPSESSIONMARKER
if isLPT==1 ;  dio=diosend(dio,[AD.LPTsessionStop   5 ]);  end

