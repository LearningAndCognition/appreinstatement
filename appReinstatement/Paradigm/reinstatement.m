
% REINSTATEMENT
disp('REINSTATEMENT');

settextstyle('',35);
clearpict(2);
preparestring('+',2);
 drawpict(2);
 
 %% ITIs
 if day==3 %outside scanner
     itiRI=[4 8 13]*1000;
 elseif day==4 && reinstatementNumber==1 %scanner pre
     itiRI=[6 12 7]*1000;
 elseif day==4 && reinstatementNumber==2 %scanner in between
     itiRI=[10 6  9]*1000;
 end

% itiRI= itiRI(randperm(length(itiRI)));



% wait(4000);
for i=1: length(itiRI)
%     disp('pumpe an')
    wait(itiRI(i));
    if isPump==1
    [pid]=syringepump_ini(3,1);
    else
      disp('..NO PUMP connected')  ;
    end
    
end


wait(4000);
%% ad little time after 2nd part reinstatement in fmri
if day==4 && reinstatementNumber==2 %scanner in between
    wait(4000);
end