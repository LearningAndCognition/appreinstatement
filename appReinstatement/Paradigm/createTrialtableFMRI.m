function [x errors]=createTrialtable(pbid_numeric,pbid,varargin)
% example
% [x errors]=createTrialtable('1',k01,'')
% ..create c01 database 
errors=0;
x.pbid_numeric=pbid_numeric;
x.pbid=pbid;


% if nargin==3
%    x=varargin{1} ;% use predefined struct
% end
% clc

% pbid_numeric=4%1:12
x.pbclass=  mod(pbid_numeric,4); 
x.pbclass(x.pbclass==0)=4;

% ton-stim-table
%   11    12
%   21    22

[x.figWithoutcome   x.tonWithoutcome  ]=ind2sub([2,2],x.pbclass);


% table
% [1] welcher stim ist cs
% [2] ton ausbalanziert
%
% [3] same stimulus max 3x
% [4] left right ausbalanciert
% % % %  left/right  (beschränken: max 3 same) -?
% [5] outcome: 50%  , beschränkung:  max 3   ?anfang: 1-1-0-1-0
% [6] outcomes ausbalanziert für jede seite
%
% aufgabe: 1 bild pro trial , pbn gibt an wo das kommt (location)



if 1
    try
        rand('state', sum(100*clock));
    catch
        rand('twister',sum(100*clock));
    end

    x.outcomeprob=.5;
    x.ntrials    =120;
    x.ncondition =2;

 


%______________________________________________________________________
%%        mother trials                                                             
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ

    %code possible scenarios
    %S H O  :[s]stimulus(fig1/2), [H]Hemisphere(L/R), [O]Outcome(yes/no)
    tb=[
        1 1 1
        1 1 0
        1 2 1
        1 2 0
        2 1 0
        2 1 0
        2 2 0
        2 2 0
        ];
    
    tb1= repmat(tb,[7  1]);

end




%______________________________________________________________________
%%      pseudo-randomize first part                (56trls)                                             
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ

tb2=tb1( randperm(size(tb1,1)),:);
ok=0;
nwhile =0;

while ok==0
nwhile   =nwhile+1;
loopagain=0;

   tb2=tb2( randperm(size(tb2,1)),:);
%     cntmax=[3 3 3];
    cntmax=[3 3 2];%[s-h-o]
    statebef=tb2(1,:);
    cnt=[1 1 statebef(3)];

%     disp([tb2(i-1,:)   ]);
    for i=2:size(tb2,1)
        state=tb2(i,:);
        if state(1)==statebef(1)
            cnt(1)=cnt(1)+1;
        else
            cnt(1)=1;
        end
        if state(2)==statebef(2)
            cnt(2)=cnt(2)+1;
        else
            cnt(2)=1;
        end
        statebef=state;
        if state(3)==1 %seltenes erreignis (outcome)
            cnt(3)=cnt(3)+1;
        elseif state(3)==0
            cnt(3)=0;
        end
        %    disp([tb2(i-1,:)   ]);
%         disp([tb2(i,:)   cnt ]);

%         pause
        if sum(cnt>cntmax)>0
           loopagain=1;
           break;
        end
    end
    
    if tb2(1,1)  ==2; loopagain=1;end
    if tb2(end,1)==1; loopagain=1;end
    
    if loopagain==0
    ok=1;
    end
end

tb2([1 end],:);
% tb2

% return

%______________________________________________________________________
%%      startertrials                                                               
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ

istart=[1 8 3  5  ]; %first 4-predefined trials
tbstart=tb(istart,:  );

irest=setdiff(1:8,istart); %remnant trials
tbrest=tb(irest,:  );


%______________________________________________________________________
%%          attach 2nd block as copy                                                           
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ

tb3=[tbstart; tb2; (tbrest); tb2];
tb3=[ [1:size(tb3,1)]' tb3];%add trialnum



%______________________________________________________________________
%%        define fig for outcome                                                          
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
if x.figWithoutcome==2 %now the other figure is linked with the output
   tb3(:,2)= abs((([ tb3(:,2)])-3 ));
end

%______________________________________________________________________
%%       add tone                                                           
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ

x.tones=[400 500] ;%hz  frequencies
%add tone
itone_outcome  =find(tb3(:,2)==x.figWithoutcome);
itone_nooutcome=setdiff([1:size(tb3,1)]', itone_outcome  );
tb3(itone_outcome, 5  )    =   x.tones(x.tonWithoutcome )   ;%this tone is linked with fig for outcome
tb3(itone_nooutcome, 5  )  =   x.tones(setdiff(1:2,x.tonWithoutcome) )  ;%tone wos nix gibt

%______________________________________________________________________
%%       add LPTmarker for outputtrials vs noOutputtrials                                                           
%ŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻŻ
x.lptmarker={'outcome'   [4] 'trial with figure assoc with [outcome]   get this marker';...
             'nooutcome' [8] 'trial with figure assoc with [NOoutcome] get this marker' }
tb3(itone_outcome, 6  )    =   x.lptmarker{1,2}   ;%fig with outcome 
tb3(itone_nooutcome, 6  )  =   x.lptmarker{2,2}  ;% fig without outcome


%     
%     
% disp('first 10 trls');
% disp(tb3(1:10,:));
% disp('last 10 trls');
% disp(tb3(end-9:end,:)); 

x.tbhead={'[1]TRLnum' '[2]picture' '[3]LRlocation' '[4]outcome' '[5]tone' '[6]LPTmrk'};
x.trialrestriction=cntmax;
% disp(x);
%add tone
if pbid_numeric==0 %TRAINING
    x.tb=flipud(tb3(end-10:end,:));
    x.tb(:,4)=0; %no OUtcome in traoing
    x.tb(:,5)=x.tb(:,5)*1.5 ;%change tones by factor 1.5
    x.tb(:,6)=0             ;%send no marker to LPT
    
    x.exp='training';
    x.stimulustoken='tt*.jpg';
    x.stimulusinstr='instr_train.jpg';
    x.istraining=1;
    
    x.dorating=0; % do/do not rating
    x.dopuzzle=0; % do/do not puzzle
    
    disp('...presenting: >>>TRAINING');
else
    x.tb    =tb3;
    x.exp='exp';
    x.stimulustoken='tb*.jpg';
    x.stimulusinstr='instr.jpg';
    x.istraining=0;
        
    x.dorating=1; % do/do not rating
    x.dopuzzle=1; % do/do not puzzle
    
    disp('...presenting: >>>EXPERIMENT');
end


load iti;%(varname: iti)
x.tb(:,7)=iti(1:size(x.tb,1));    x.tbhead{7}='iti';


%% 2nd and 3rd day
c=x.tb;
i4=find(c(:,6)==4);
i8=find(c(:,6)==8);

%use 2nd half size
nTrlcond=size(c,1)/(2+2);
tb2=x.tb(sort([i4(1:nTrlcond) ; i8(1:nTrlcond)]),:);
% tb2=x.tb(sort([i4(nTrlcond+1:end) ; i8(nTrlcond+1:end)]),:);
tb2(:,4)=0 ;%no outcome
% tb2(:,7)=iti(1:size(tb2,1));
%constant ITI


tb3=tb2; %BACKUP for day3

%% #############################
%% 2nd day
%% #############################
% n2balance=6;
% balanceID=rem(pbid_numeric,n2balance);
% balanceID(balanceID==0)=n2balance;
% 
% %% flip table x,x,x[4,5,6],x,x,x,[10,11,12],x,x,x,...
% % if rem(pbid_numeric,4)==1 | rem(pbid_numeric,4)==2
% if balanceID>=4  

%  tb2=x.tb(sort([i4(nTrlcond+1:end) ; i8(nTrlcond+1:end)]),:);
% tb2(:,4)=0 ;%no outcome
%   tb2(:,7)=iti(1:size(tb2,1));
% tb2(:,1)=1:size(tb2,1);

%% [x x x x]5 6 7 8 [x x x x]...
n2balance=8;
balanceID=rem(pbid_numeric,n2balance);
balanceID(balanceID==0)=n2balance;

if balanceID>4
    tb2=[tb2(2:end,:); tb2(1,:)  ]; 
     tb2(:,1)=1:size(tb2,1);
end

 


%% #############################
%% 3rd day
%% #############################
%% flip table [1 2 ]x,x,[5,6],x,x,...
n2balance=4;
balanceID=rem(pbid_numeric,n2balance);
balanceID(balanceID==0)=n2balance;

% fliporder again in every 3th and 4th SUBJECT
%test rem([1:20],4)
starttrials=tb3(1:4,:);
resttrials =tb3(5:end,:);

%(1)statertrialbehandlung
starttrials=sortrows(starttrials,6);%sort accord condition
if balanceID<3 
    if balanceID==1
        starttrials=starttrials([1 3 2 4],:) ;
    else
        starttrials=starttrials([2 4 1 3],:);
    end
        
else
    if balanceID==3
        starttrials=starttrials([3 1 4 2],:);
    else
        starttrials=starttrials([4 2 3 1],:);
    end
end

try
    %(2)antiTrialSequenceEffect
    markerseq  =[starttrials(end,6) resttrials(1,6)];
    if markerseq(1)==markerseq(2);
        mrkneeded=setdiff(unique(starttrials(:,6))', markerseq(1));

        ineed=find(resttrials(:,6)==mrkneeded);
        itake=min(find(diff(ineed)==1));
        idxMrkchange=ineed(itake);

        starttrials=[starttrials ; resttrials(idxMrkchange,:)  ];
        resttrials(idxMrkchange,:)=[];
    end
end

tb3=[starttrials; resttrials];
tb3(:,1)=1:size(tb3,1);



 

%% check location
x.check.location_tb2=[...
  4 1  length(find(tb2(:,6)==4 & tb2(:,3)==1))
  4 2  length(find(tb2(:,6)==4 & tb2(:,3)==2))
  8 1  length(find(tb2(:,6)==8 & tb2(:,3)==1))
  8 2  length(find(tb2(:,6)==8 & tb2(:,3)==2))
    ];
x.check.time_tb3ITI=sum(tb3(:,7));
disp(['timeITI: ' num2str(x.check.time_tb3ITI) ]);

%% extract day1 from dataBASE
tb1=x.tb;

%% clean dataBASE
try; x=rmfield(x,'dorating');end
try; x=rmfield(x,'istraining');end
try; x=rmfield(x,'dopuzzle');end
try; x=rmfield(x,'exp');end
 
%% add stuff
x.completed=0; %not completed

%% save dataBASE
padata=fullfile(pwd,'data');

writeDATA=1;

if exist(fullfile(padata, [ pbid '_day1.mat']  ))==2
   [answ ]= questdlg([[ pbid '_day1 & '  pbid '_day2 & '  pbid '_day3 ' ]  'existieren bereits' char(10)...
       '[NEU ERSTELLEN] sollen diese neu erstellt werden (dann gehen womöglich bestehende daten verloren)  ' char(10)...
       '...andernfalls...' char(10),...
       '[ABBRUCH]: bricht programm komplett ab, ältere files mit selber PBN-ID ' ...
       'müssen händisch gelöscht bz verschoben werden'],....
       ['ACHTUNG'],...
       'NEU ERSTELLEN','ABBRUCH','default') 
   
   if strcmp(answ,'ABBRUCH')
     writeDATA=0;
     errors   =1;
     disp(' no data created  ');
     disp('program stopped');
    return
   end
end

   
   if writeDATA==1;
       x.tb    =tb1;
       x.day   =1;
       x.daystr='day1'
       x.tb_backup=tb1;
       save(fullfile(padata, [ pbid '_' x.daystr ]  ),'x');

       x.tb    =tb2;
       x.day   =2;
       x.daystr='day2';
        x.tb_backup=tb1;
       save(fullfile(padata, [ pbid '_' x.daystr ]  ),'x');

       x.tb    =tb3
       x.day   =3;
       x.daystr='day3';
        x.tb_backup=tb1;
       save(fullfile(padata, [ pbid '_' x.daystr ]  ),'x');
       
       
       x.tb    =tb3
       x.day   =4;
       x.daystr='day4';
       x.tb_backup=tb1;
       save(fullfile(padata, [ pbid '_' x.daystr ]  ),'x');
       
      disp([' ' pbid  '_day1,2,3 created  ']);
      
      pause(1); drawnow;
   end








