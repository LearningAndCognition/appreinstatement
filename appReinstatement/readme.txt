

The experimental task is programmed using Cogent toolbox (Wellcome Department of Imaging Neuroscience, Institute of
Neurology, London, UK) 
- for convenience the Cogent toolbox is delivered 

#TESTS
========================
- experiment has been tested using windows 7 & 10 (32 and 64 bit version)

#Installation & RUNNING
========================
- copy "appReinstatement" with subdirectories to the desired path
- change Matlabs current working directory to subdirectory "Paradigm"
- type 'starter' in Matlab's comandline 
- note that starter.m adds the path of the Cogent toolbox, thus no paths of cogent must be set in Matlab in advance 

- after executing starter.m a GUI is asking for the user ID (such as '01' or '09' ) and the session (1,2,3,4). Note that sessions 1 and 2 refers
  to the sessions of day 1 and 2, respectively, whereas session 3 and 4 refer to the startle session at day 3 (outside scanner) and session in the 
  scanner (day 3). The trial randomizaion is done at day-1 for all three days, thus it is mandatory to start with day-1.
- depending on the day the session starts with an intro screen followed by Ratings, a startle task or the forced choice task. Please consult 'starter.m' 
  for the used keys. 
- * task in scanner (session 4, day3): Note that experiment starts when the first trigger arrives. In the current version it is marker '5'. When testing this task outside  
  the scanner environment, don't forget to press key '5' to jump out of the 'wait for MR marker'-loop




# further Information
========================
- type 'edit starter' in Matlabs comandline to change/modify setting parameter
- settings can be changed in the starter.m file
- In the current version the parallelport (used for sending markers to other devices) and the serial port (used for the syringe pump) is disabled --> see 'starter.m'
- see also help of starter.m


# Potential conflicts
========================
- see help of starter.m
- depending of the type of conflict you may have to change the screen-resolution parameter and used keyboard keys 

