%checkerboard
clear close all
%%%%%%%%%%%%%%%%    changable paras     %%%%%%%%%%%%%%%%%%%%%%%
freq=8; %[HZ]
stimtime=20; %EACH HEMISIDE per trial [s]
resttime=20
Ntrials=20;
Nblocks=1;
%pin_left=4;  %LPT1_pin
% pin_right=4;
% pin_stop=8;

pinon=255;
pinoff=0;
markerONtime=30;%[ms]
cyclesPERsec=ceil(1000/((1000/freq)*2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

config_data( 'C:\matlab_download\cogent\expfiles\ganzfeld.dat' );
t_pic=124;%floor(1000/freq)      ;  %time for each checkerboard each [ms]

%___________old stuff
% config_display( 1 )
config_display( 1, 2,  [0 0 0],[1 1 1], 'ARIAL', 25, 5 ); % DISPLAY
config_keyboard(2,1, 'exclusive' );% KEYVBOARD 
% This is equivalent to:
% config_display( 0, 2, [0 0 0], [1 1 1], 'Arial', 5, 8 ); % DISPLAY
% config_display(0, 1, 0, 1, 'Arial', 50, 5, 8)
% config_display(3, 1, [0 0 0], [1 1 1], 'Helvetica', 25, 8,32);

%===========loading/setup
fileLI1 = getdata( 1, 1 );
fileLI2 = getdata( 2, 1 );


%=====================PARALLELPORT-==========================
% dii=digitalio('parallel',1); %INPUT: initiates LPT1
% addline (dii,9:12,'in');
% % 		putvalue (dii,0)

dio=digitalio('parallel',1); %OUTPUT: initiates LPT1
addline(dio,0:7,'out');
putvalue(dio,0);


%=====================  delay       -==========================
delay=repmat([0 500 1000],1,100)';
logtime=zeros(1000,2);

%===============START=======================
start_cogent;
clearpict( 1 );
loadpict( fileLI1, 1 );
loadpict( fileLI2, 2 );
%===========    FIXED BUFFERLOADINGS     ===========
% ------------------------------------------------------
%PAUSE
settextstyle( 'Arial', 35 ) 
preparestring( 'PAUSE: weiter mit <SPACE>', 5) ; %BUFFER 5
%----BUFFER
drawpict( 5 );
drawpict( 1 );
drawpict( 2 );


tic
%====================      STARTS Praesentation         ====================
%PAUSES
Nco=0;
Nevents=6;
 tic; 
 
for BL=1:Nblocks
  
  
  drawpict( 5 ); %displays PAUSE-SCREEN
  map = getkeymap;  waitkeydown( inf,map.Space ); %- wait for space key to be pressed
  clearpict(0);
  drawpict( 0 ); %displays BLANK-screen
  clearkeys;
  
  for TR=1:Ntrials
    
 %================== BREAKING condition =========================================================   
    % Clear all key events
    readkeys; % Read all key events since CLEARKEYS was called
    if isempty(getkeydown)~=1 
      [ ABORTdum(:,1),ABORTdum(:,2),ABORTdum(:,3) ] = getkeydown;
      if ABORTdum(:,1)==17 %quits with key [Q]: 17
        clear ABORTdum;
        stop_cogent;
        error(' ....abort due to keypress <q> .........')  ;
      end
    end
    
    
    
    %==================CHECKSTART=========================================================
    Nco=Nco+1;
    
    %=======control 
           
 
            logtime((Nco*Nevents)-Nevents+1 ,:)=[toc 1];%
    putvalue(dio,pinoff);
            logtime((Nco*Nevents)-Nevents+2 ,:)=[toc -1]; %- logtime((Nco*Nevents)-Nevents+1 ,1);
    
    
    drawpict( 1 ); % draws stable control-CHECK
    wait(0  );% waits for 2sec in order to avoid pins formepur 'high'-status   
    
    
    if 0
        while  binvec2dec(getvalue(dii)) ==5; %waits as long as 'PIN' is 'LOW'
        end
        
    end
    
    pause(resttime);
    
    
    
    
            logtime((Nco*Nevents)-Nevents+3 ,:)=[toc 20]; %-logtime((Nco*Nevents)-Nevents+2 ,1);
    wait(delay(Nco))  ; %waits for delaytime before check start
            logtime((Nco*Nevents)-Nevents+4 ,:)=[toc 21]; %- logtime((Nco*Nevents)-Nevents+3 ,1);
    
    
    %=====CHECKERBOARD
           putvalue(dio,pinon);wait(400);putvalue(dio,0);%wait(400);
    logtime((Nco*Nevents)-Nevents+5 ,:)=[toc 1];%- logtime((Nco*Nevents)-Nevents+4 ,1);
    
    for i = 1: cyclesPERsec * stimtime %* Ncycles_for_wholetime  % cycles for 1 sec* total time    
      drawpict( 1 );
      wait( t_pic );
      drawpict( 2 );
      wait( t_pic );
    end
    
    
    putvalue(dio,pinon);wait(400);putvalue(dio,0);%wait(400);
     logtime((Nco*Nevents)-Nevents+6 ,:)=[toc 333];% %- logtime((Nco*Nevents)-Nevents+5 ,1);
    
    %=====MARKER
    
    
  end %Ntrials
end %Nblocks
%==================CHECKSTOPP=========================================================
%====================      ENDS Praesentation         ====================
 



putvalue(dio,pinoff);%sets marker to 0
stop_cogent;

%==============END================================
logtime(:,3)=[0; diff(logtime(:,1))]; %DIFFERENCES