
clear

% sample_negprime - negative priming


%===========================================================================

a=150
b=150

pos1a=-1*a; pos1b= 1*b;
pos2a= 1*a; pos2b= 1*b;
pos3a=-1*a; pos3b=-1*b;
pos4a= 1*a; pos4b=-1*b;

loc=[-1*a  1*b;   1*a  1*b;     -1*a  -1*b;    1*a  -1*b ]

%===========================================================================
k=2,
b=[ones(k,1)*1;ones(k,1)*2;ones(k,1)*3;ones(k,1)*4 ]
dum=1:4
FIRST=shuffle(b)
c=repmat(dum,length(b),1)
for i=1:size(c,1)
  c(i,FIRST(i))=0;
  dum2=find(c(i,:)~=0);
  cnew(i,:)=    c(i,dum2);
end

SECOND=(shuffle(cnew'))';
SECOND=SECOND(:,1);
TabSTIM=([FIRST SECOND])

myfont='Garamond';%Verdana
%===========================================================================
% config_display(0)
config_display( 1, 2, [.7 .7 .7], [1 1 1], myfont, 25, 4 )
% config_keyboard
% config_keyboard(1,5, 'nonsexclusive' );%      config_keyboard( quelength = 100, resolution = 5, mode = 'exclusive' )
config_keyboard(1,1, 'exclusive' );%      config_keyboard( quelength = 100, resolution = 5, mode = 'exclusive' )
% config_data( 'sample6.dat' );
config_log('sample_negprime.log' );%C:\matlab_download\cogent\Cogent2000v1.25\Samples\
start_cogent;
%===========================================================================
% pause


setforecolour( 0,  0,  0   )% - set foreground colour to white
SETTEXTSTYLE( myfont, 50 )
preparestring( '+', 2); % Draw fixation point in display buffer 2
setforecolour( .6, .6,  .6   )% - set foreground colour to whiteS
SETTEXTSTYLE( myfont, 50 )
preparestring( char(1), 2 , pos1a,  pos1b); % Draw fixation point in display buffer 2
preparestring( char(1), 2 , pos2a,  pos2b); % Draw fixation point in display buffer 2
preparestring( char(1), 2 , pos3a,  pos3b); % Draw fixation point in display buffer 2
preparestring( char(1), 2 , pos4a,  pos4b); % Draw fixation point in display buffer 2
drawpict( 2 ); 


for i=1:size(TabSTIM,1)
reiz=i;
% reiz=4
%rawpict( 2 );  % Display fixation point and wait 1500ms
%t0 = drawpict( 0 ); % Display word and get time
%===========================================================================
setforecolour(0,0,0   )% - set foreground colour to white
SETTEXTSTYLE( myfont, 50 )
preparestring( '+', 3); % Draw fixation point in display buffer 2

setforecolour( .6, .6,  .6   )% - set foreground colour to whiteS
SETTEXTSTYLE( myfont, 50 )
preparestring( char(1), 3 , pos1a,  pos1b); % Draw fixation point in display buffer 2
preparestring( char(1), 3 , pos2a,  pos2b); % Draw fixation point in display buffer 2
preparestring( char(1), 3 , pos3a,  pos3b); % Draw fixation point in display buffer 2
preparestring( char(1), 3 , pos4a,  pos4b); % Draw fixation point in display buffer 2

SETTEXTSTYLE( myfont, 40 )
setforecolour( 0,   0,   0   )% - set foreground colour to bright red
% preparestring('o', 3 ,pos1a, pos1b); % Draw fixation point in display buffer 2
% preparestring('x', 3 ,pos2a, pos2b); % Draw fixation point in display buffer 2
preparestring('o', 3 ,loc(TabSTIM(reiz,1),1), loc(TabSTIM(reiz,1),2)); % Draw fixation point in display buffer 2
preparestring('x', 3 ,loc(TabSTIM(reiz,2),1), loc(TabSTIM(reiz,2),2)); % Draw fixation point in display buffer 2



drawpict( 2 ); 
clearkeys;    % Clear all key events
wait(500)
t0=drawpict( 3 );  % Display fixation point and wait 1500ms
   str=sprintf('%d %d %d',(TabSTIM(reiz,1)),(TabSTIM(reiz,2)),t0);    % Log word and time it was displayed
   logstring(str);
 waituntil( t0+1300 );
   
    readkeys; % Read all key events since CLEARKEYS was called
    logkeys; % Write key events to log
 

clearpict( 3 );
% clearpict( 2 );
%t0 = drawpict( 3 ); % Display word and get time
end

%===========================================================================
if 0
% for i = 1:countdatarows
   
   word = getdata( i, 1 );    
   clearpict( 1 );
   preparestring( word, 1 ); % Draw word in display buffer 1
   
   drawpict( 2 );  % Display fixation point and wait 1500ms
   wait( 1500 );
 
   clearkeys;    % Clear all key events
   t0 = drawpict( 1 ); % Display word and get time
   
   str=sprintf('%s: %d',word,t0);    % Log word and time it was displayed
   logstring(str);
   
   waituntil( t0+500 );    % Wait until 500ms after word was presented
   
   % Clear screen and wait until 1000ms after word was presented
   drawpict( 3 );  
   waituntil( t0+1000 );
   
   % Read all key events since CLEARKEYS was called
   readkeys;
   
   % Write key events to log
   logkeys;
end
%===========================================================================s

stop_cogent;

if 0
edit C:\matlab_download\cogent\Cogent2000v1.25\Samples\sample_negprime.log

r=loadlog('sample_negprime.log');%rr=r{6}{1} %6.zeile ist 1.datenzeile
r{6}
% 0	[0]	:	COGENT START
% 519	[519]	:	1 4 519
% 1819	[1300]	:	Key	11	UP	at	953     
% 2330	[511]	:	2 4 2330
% 3630	[1300]	:	Key	11	UP	at	3069    
% 4142	[512]	:	3 1 4142
% 5948	[1806]	:	2 3 5948
% 7754	[1806]	:	4 1 7754
% 9560	[1806]	:	3 4 9560
% 10860	[1300]	:	Key	11	UP	at	9642    
% 11378	[518]	:	1 3 11378
% 12678	[1300]	:	Key	11	UP	at	11987   
% 13191	[513]	:	4 2 13191
% 14491	[1300]	:	Key	11	UP	at	13853   
% 14492	[1]	:	COGENT STOP
end