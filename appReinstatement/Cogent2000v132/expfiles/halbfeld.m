%checkerboard
clear close all
%%%%%%%%%%%%%%%%    changable paras     %%%%%%%%%%%%%%%%%%%%%%%
freq=8; %[HZ]
stimtime=10; %EACH HEMISIDE per trial [s]
Ntrials=3;
Nblocks=2;
pin_left=2;  %LPT1_pin
pin_right=4;
pin_stop=8;
markerONtime=30;%[ms]
cyclesPERsec=ceil(1000/((1000/freq)*2));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

config_data( 'C:\matlab_download\cogent\expfiles\halbfeld.dat' );
t_pic=floor(1000/freq)      ;  %time for each checkerboard each [ms]

%___________old stuff
% config_display( 1 )
config_display( 1, 1,  [0 0 0],[1 1 1], 'ARIAL', 25, 5 ); % DISPLAY
config_keyboard(2,1, 'exclusive' );% KEYVBOARD 
% This is equivalent to:
% config_display( 0, 2, [0 0 0], [1 1 1], 'Arial', 5, 8 ); % DISPLAY
% config_display(0, 1, 0, 1, 'Arial', 50, 5, 8)
% config_display(3, 1, [0 0 0], [1 1 1], 'Helvetica', 25, 8,32);

%===========loading/setup
fileLI1 = getdata( 1, 1 );
fileLI2 = getdata( 2, 1 );
fileRE1 = getdata( 3, 1 );
fileRE2 = getdata( 4, 1 );
dio=digitalio('parallel',1);
addline(dio,0:7,'out');
putvalue(dio,0);
%===============START=======================
start_cogent;
clearpict( 1 );
loadpict( fileLI1, 1 );
loadpict( fileLI2, 2 );
loadpict( fileRE1, 3 );
loadpict( fileRE2, 4 );

%===========    FIXED BUFFERLOADINGS     ===========
% ------------------------------------------------------
%PAUSE
settextstyle( 'Arial', 35 ) 
preparestring( 'PAUSE: weiter mit <SPACE>', 5) ; %BUFFER 5
%----BUFFER
drawpict( 5 );
drawpict( 1 );
drawpict( 2 );
drawpict( 3 );
drawpict( 4 );

tic
%====================      STARTS Praesentation         ====================
%PAUSES

for BL=1:Nblocks
  
  
  drawpict( 5 ); %displays PAUSE-SCREEN
  map = getkeymap;  waitkeydown( inf,map.Space ); %- wait for space key to be pressed
  clearpict(0);
  drawpict( 0 ); %displays BLANK-screen
  clearkeys;
  
  for TR=1:Ntrials
  
    
    % Clear all key events
    readkeys; % Read all key events since CLEARKEYS was called
    if isempty(getkeydown)~=1 
      [ ABORTdum(:,1),ABORTdum(:,2),ABORTdum(:,3) ] = getkeydown;
      if ABORTdum(:,1)==17 %quits with key [Q]: 17
        clear ABORTdum;
        stop_cogent;
        error(' ....abort due to keypress <q> .........')  ;
      end
    end
    
    
    
    %LEFT
    putvalue(dio,pin_left);
    wait(markerONtime);
    
    %=====CHECKERBOARD
    for i = 1: cyclesPERsec * stimtime %* Ncycles_for_wholetime  % cycles for 1 sec* total time    
      drawpict( 1 );
      wait( t_pic );
      drawpict( 2 );
      wait( t_pic );
    end
    %=====MARKER
    putvalue(dio,pin_stop);
    wait(markerONtime);
    putvalue(dio,0);
    
    %RIGHT
    putvalue(dio,pin_right);
    wait(markerONtime);
    
    %=====CHECKERBOARD
    for i = 1: cyclesPERsec * stimtime %* Ncycles_for_wholetime  % cycles for 1 sec* total time    
      drawpict( 3 );
      wait( t_pic );
      drawpict( 4 );
      wait( t_pic );
    end
    %=====MARKER
    putvalue(dio,pin_stop);
    wait(markerONtime);
    putvalue(dio,0);
    
  end %Ntrials
end %Nblocks
%====================      ENDS Praesentation         ====================
toc
stop_cogent;

%==============END================================
% start_cogent;
% 
% x=[1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2]
% % for i = 1:size(x,2)%countdatarows
%    file1 = getdata( x(1), 1 );
%    file2 = getdata( x(2), 1 );
%       clearpict( 1 );
%    loadpict( file1, 1 );
%    loadpict( file2, 2 );
%    for i = 1:100  %countdatarows    
%     drawpict( 1 );
%   wait( 20 );
%    drawpict( 2 );
%   wait( 20 );
% end
% 
% stop_cogent;
