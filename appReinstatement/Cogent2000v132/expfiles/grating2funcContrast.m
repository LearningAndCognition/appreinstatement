function grating2funcContrast(monfreq,time2play,balks,veloc,movinggrat,...
    ca1,ca2,ca3,cb1,cb2,cb3);
% function grating2funcContrast(monfreq,time2play,balks,veloc,movinggrat,...
%     ca1,ca2,ca3,cb1,cb2,cb3)
% clear x y w h
% cgloadlib;
% cgopen(1,0,0,1)
% monfreq=56;
% balks=200;
% veloc=8;
% time2play=15
%ca1,ca2,ca3,cb1,cb2,cb3,=RGBcolor

monres=1;
if monres==1
    pixh=1280;
    pixv=1024;
end


timcyc1=1/monfreq *length(1:veloc:balks);
timcyc2=round(time2play/timcyc1);
% cgflip(1,0,0);

if movinggrat==1
    for j=1:timcyc2
        for i=1:veloc:balks
%             x=-320*4+i:balks:+320+i;
%             y(1,1:length(x))=-240;
%             w(1,1:length(x))=balks/2;
%             h(1,1:length(x))=960;%1100

            x=-pixh+i:balks:+pixh+i;
            y(1,1:length(x))=0;
            w(1,1:length(x))=balks/2;
            h(1,1:length(x))=pixv;%1100
            cgpencol(cb1,cb2,cb3);cgrect(x,y,w,h);
            cgpencol(1,0,0);cgpenwid(3);cgdraw(0,5,0,-5);cgdraw(5,0,-5,0);
            cgflip(ca1,ca2,ca3);cgflip('v');
            
            
        end
    end
else
    for j=1:timcyc2
        for i=1:veloc:balks
%             x=-320*4:balks:+320*4;
            x=-pixh/2:balks:+pixh/2;
            y(1,1:length(x))=0;
            w(1,1:length(x))=balks/2;
            h(1,1:length(x))=pixv;%1100;%960;
            cgpencol(cb1,cb2,cb3);cgrect(x,y,w,h);
            cgpencol(1,0,0);cgpenwid(3);cgdraw(0,5,0,-5);cgdraw(5,0,-5,0);
            cgflip(ca1,ca2,ca3);cgflip('v');
            
            
        end
    end
    
end
% toc

% cgshut
%===========================================================================
%===========================================================================
%===========================================================================







