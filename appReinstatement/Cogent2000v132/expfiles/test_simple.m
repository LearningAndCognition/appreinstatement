% SAMPLE2 - Visual presentation of pictures at a constant rate
%config_display( 0 )

%generation of a pseudo-randomized array of conditions.


dio=digitalio('parallel',1);




% configuration section
config_display(0,6, [0 0 0], [1 1 1], 'Helvetica', 25, 8);
config_log('test.log');
% config_sound( 1, 16, 11025, 100);

    
% Specifiy data file to find all pictures to be shown;
config_data('C:\matlab_download\cogent\Cogent2000v1.25\Samples\simple_test.dat');

    %matlab configuration of parallel output (lines 0:7)
    addline(dio,0:7,0,'out');


%start cogent
start_cogent;

% preparepuretone(440,1000,1);
% playsound(1);
% waitsound(1);
% 

     stimulus = getdata( 1, 1 );
        %is this necessary? i dont know...
%        clearpict( 1 );
        
        loadpict( stimulus, 1 );
        %here the stimulation conditions are presented (30 times)
    for t=1:1000       
%       
       
        % sends parallel port code of stimulation condition
 
        drawpict( 1 );
        drawpict( 5 );

        %waituntil( rand_s(1,2) + t1_alter );
        
        
        
    end
    

% tone is sent to indicate end of block
preparepuretone(440,1000,2);    
playsound(2);
waitsound(2);    
    
end

stop_cogent;
