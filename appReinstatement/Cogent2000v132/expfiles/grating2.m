






clear x y w h
cgloadlib;
cgopen(1,0,0,1)


monfreq=56;
balks=200;
veloc=8;
time2play=15


timcyc1=1/monfreq *length(1:veloc:balks)
timcyc2=round(time2play/timcyc1)

cgflip(1,0,0);
tic
for j=1:timcyc2
    for i=1:veloc:balks
        x=-320*4+i:balks:+320+i;
        y(1,1:length(x))=-240;
        w(1,1:length(x))=balks/2;
        h(1,1:length(x))=960;
        cgpencol(0,1,0);cgrect(x,y,w,h);
        cgpencol(0,0,0);cgpenwid(1);cgdraw(0,10,0,-10);cgdraw(10,0,-10,0);
        cgflip(1,0,0);cgflip('v');
        
        
    end
end
toc

cgshut
%===========================================================================
%===========================================================================
%===========================================================================

%===========================================================================

if 0
    
    cgopen(1,0,0,0);
    
    clear x y w h
    
    ksize=50
    m=0
    % for j=1:1
    for i=1:1:50
        m=m+1;
        cgmakesprite(m,640,480,1,0,0)
        cgsetsprite(m) 
        
        x=-320*4+i:ksize:+320+i;
        y(1,1:length(x))=-240;
        w(1,1:length(x))=ksize/2;
        h(1,1:length(x))=960;
        
        cgpencol(0,1,0);
        cgrect(x,y,w,h);
        
        cgpencol(0,0,0);cgpenwid(1);cgdraw(0,10,0,-10);cgdraw(10,0,-10,0);
        % cgflip(1,0,0);cgflip('v');
        
        
    end
    % end
    
    cgsetsprite(0)
    cgflip(1,1,1)
    for j=1:3
        for i=1:1:m
            cgdrawsprite(i,0,0);
            cgflip(1,1,1);
            cgflip('v');%pause
        end
    end
    
    
    cgshut
    
    
    cgsetsprite(0)
    cgflip
    for j=1:5
        for i=1:(m-1)-12
            cgdrawsprite(i,0,0);      cgflip(1,1,1);     cgflip('v'),%pause
        end,
        
    end
    %===========================================================================
    cgloadlib
    cgopen(1,0,0,0)
    cgmakesprite(78,320,240,0,0,0)
    cgsetsprite(78)
    
    x = [-50 0 50];
    y = [-25 61 25];
    cgpencol(1,0,0)
    cgrect(0,0,160,120)
    cgpencol(0,1,0)
    cgellipse(0,0,160,120,'f')
    cgpencol(1,1,0)
    cgpolygon(x,y)
    cgpencol(0,0,1)
    
    cgmakesprite(79,320,240,0,0,0)
    cgsetsprite(79)
    
    cgdraw(-160,120,160,-120)
    cgdraw(-160,-120,160,120)
    cgpencol(1,1,1)
    cgdraw(-90,0)
    cgdraw(90,0)
    cgdraw(0,-70)
    cgdraw(0,70)
    cgpencol(0,0,0)
    cgfont('Arial',14)
    cgtext('To sleep, perchance to dream',0,0)
    
    
    cgsetsprite(0)
    cgflip(0,0,0)
    cgdrawsprite(78,0,0)
    cgflip(0,0,0)
    
    cgdrawsprite(79,0,0)
    cgflip(0,0,0)
    
    %===========================================================================
    cgloadlib
    cgopen(1,0,0,0)
    
    for i=30:50;
        cgmakesprite(i,320,240,0,0,0);
        cgsetsprite(i);
        
        cgpencol(0,1,0);
        cgdraw(-160+i,120,160+i,-120);
        cgdraw(-160,-120,160,120);
        
        
    end
    
    cgsetsprite(0)
    cgflip(0,0,0)
    for i=30:50;
        cgdrawsprite(i,0,0)
        cgflip(0,0,0)  
    end
    
    
    cgdrawsprite(78,0,0)
    cgflip(0,0,0)
    
    cgdrawsprite(79,0,0)
    cgflip(0,0,0)
    
    
    
    
end







