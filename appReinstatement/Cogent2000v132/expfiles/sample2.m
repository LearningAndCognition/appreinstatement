% SAMPLE2 - Visual presentation of pictures at a constant rate
%config_display( 1 )

% This is equivalent to:
%config_display(0, 1, 0, 1, 'Ariel', 50, 4, 8)
 config_display(1, 1, [0 0 0], [1 1 1], 'Helvetica', 25, 8,32);
%config_display(1);
% Specifiy data file to be 'sample1.dat;
config_data( 'c:\matlab_download\cogent\cogent2000v1.25\samples\sample2blackUwhite.dat' );
%config_data( 'sample2plus.dat' );
% config_data( 'sample2plus.dat' );
start_cogent;

x=[1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2]
% for i = 1:size(x,2)%countdatarows
   file1 = getdata( x(1), 1 );
   file2 = getdata( x(2), 1 );
      clearpict( 1 );
   loadpict( file1, 1 );
   loadpict( file2, 2 );
   for i = 1:3  %countdatarows    
    drawpict( 1 );
  wait( 200 );
   drawpict( 2 );
  wait( 200 );
end

stop_cogent;
