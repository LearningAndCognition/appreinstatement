% SAMPLE6 - Visual presentation of words, reading and logging keyboard input.
config_display(1)
config_keyboard;
config_data( 'sample6_math.dat' );
config_log( 'c:\matlab_download\cogent\cogent2000v1.25\samples\sample6_math.log' );

start_cogent;

   file1 = getdata(1, 1 );
    clearpict( 1 );
   loadpict( file1, 1 );
   drawpict( 1 );
    WAITKEYDOWN(inf, 71 ) 

preparestring( '+', 2 ); % Draw fixation point in display buffer 2

for i = 2:countdatarows
%    if i*3000>6*3000
%        break
%    end
   word = getdata( i, 1 );    
   clearpict( 1 );
   preparestring( word, 1 ); % Draw word in display buffer 1
   
   drawpict( 2 );  % Display fixation point and wait 1500ms
   wait( 500 );
 
   clearkeys;    % Clear all key events
   t0 = drawpict( 1 ); % Display word and get time
   
   str=sprintf('%s: %d',word,t0);    % Log word and time it was displayed
   logstring(str);
   
   waituntil( t0+3500 );    % Wait until 500ms after word was presented
   
   % Clear screen and wait until 1000ms after word was presented
   drawpict( 3 );  
   waituntil( t0+1000 );
   
   % Read all key events since CLEARKEYS was called
   readkeys;
   
   % Write key events to log
   logkeys;
end

stop_cogent;