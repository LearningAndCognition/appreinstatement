%checkerboard
clear close all
%%%%%%%%%%%%%%%%    changable paras     %%%%%%%%%%%%%%%%%%%%%%%
freq=8; %[HZ]
stimtime=6; %EACH HEMISIDE per trial [s]
resttime=16
Ntrials=40;
Nblocks=1;
%pin_left=4;  %LPT1_pin
% pin_right=4;
% pin_stop=8;

pinon=255;
pinoff=0;
markerONtime=3;%[ms]
cyclesPERsec=ceil(1000/((1000/freq)*2));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

config_data( 'ganzfeld1.dat' );
t_pic=124;%floor(1000/freq)      ;  %time for each checkerboard each [ms]

%___________old stuff
% config_display( 1 )
config_display( 1, 1,  [0 0 0],[1 1 1], 'ARIAL', 25, 5 ); % DISPLAY
config_keyboard(2,1, 'exclusive' );% KEYVBOARD 
%===========loading/setup
fileLI1 = getdata( 1, 1 );
fileLI2 = getdata( 2, 1 );


%=====================PARALLELPORT-==========================
dio=digitalio('parallel',1); %OUTPUT: initiates LPT1
addline(dio,0:7,'out');
putvalue(dio,0);


%=====================  delay       -==========================
delay=repmat([0 500 1000],1,100)';
logtime=zeros(1000,2);

%===============START=======================
start_cogent;
clearpict( 1 );
loadpict( fileLI1, 1 );
loadpict( fileLI2, 2 );
%===========    FIXED BUFFERLOADINGS     ===========
% ------------------------------------------------------
%PAUSE
settextstyle( 'Arial', 35 ) 
preparestring( 'PAUSE: weiter mit <SPACE>', 5) ; %BUFFER 5
%----BUFFER
drawpict( 5 );
drawpict( 1 );
drawpict( 2 );


tic
%====================      STARTS Praesentation         ====================
 tic; 
 
for BL=1:Nblocks
  
  
  drawpict( 5 ); %displays PAUSE-SCREEN
  map = getkeymap;  waitkeydown( inf,map.Space ); %- wait for space key to be pressed
  clearpict(0);
  drawpict( 0 ); %displays BLANK-screen
  clearkeys;
  
  for TR=1:Ntrials
    
 %================== BREAKING condition =========================================================   
    readkeys; % Read all key events since CLEARKEYS was called
    if isempty(getkeydown)~=1 
      [ ABORTdum(:,1),ABORTdum(:,2),ABORTdum(:,3) ] = getkeydown;
      if ABORTdum(:,1)==17 %quits with key [Q]: 17
        clear ABORTdum;       stop_cogent;
        error(' ....abort due to keypress <q> .........')  ;
      end
    end
    
    
    
    %==================CHECKSTART=========================================================
    %#####[1] pause
    putvalue(dio,128);wait(100);putvalue(dio,0);%wait(400);
    drawpict( 1 ); % draws stable control-CHECK
    pause(resttime);
     
    
 
    
    
    %%#####[2] CHECKERBOARD
    putvalue(dio,4);wait(100);putvalue(dio,0);%wait(400);
    
    for i = 1: cyclesPERsec * stimtime %* Ncycles_for_wholetime  % cycles for 1 sec* total time    
      drawpict( 1 );
      wait( t_pic );
      drawpict( 2 );
      wait( t_pic );
    end

    
    
  end %Ntrials
end %Nblocks
%==================CHECKSTOPP=========================================================
%====================      ENDS Praesentation         ====================
putvalue(dio,pinoff);%sets marker to 0
stop_cogent;
