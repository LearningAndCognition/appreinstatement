function grating2func(monfreq,time2play,balks,veloc,movinggrat)
% function grating2func(monfreq,time2play,balks,veloc,movinggrat)

% clear x y w h
% cgloadlib;
% cgopen(1,0,0,1)
% monfreq=56;
% balks=200;
% veloc=8;
% time2play=15


timcyc1=1/monfreq *length(1:veloc:balks);
timcyc2=round(time2play/timcyc1);
% cgflip(1,0,0);

if movinggrat==1
    for j=1:timcyc2
        for i=1:veloc:balks
            x=-320*4+i:balks:+320+i;
            y(1,1:length(x))=-240;
            w(1,1:length(x))=balks/2;
            h(1,1:length(x))=960;%1100
            cgpencol(0,1,0);cgrect(x,y,w,h);
            cgpencol(0,0,0);cgpenwid(1);cgdraw(0,10,0,-10);cgdraw(10,0,-10,0);
            cgflip(1,0,0);cgflip('v');
            
            
        end
    end
else
    for j=1:timcyc2
        for i=1:veloc:balks
            x=-320*4:balks:+320;
            y(1,1:length(x))=-240;
            w(1,1:length(x))=balks/2;
            h(1,1:length(x))=960;%1100;%960;
            cgpencol(0,1,0);cgrect(x,y,w,h);
            cgpencol(0,0,0);cgpenwid(1);cgdraw(0,10,0,-10);cgdraw(10,0,-10,0);
            cgflip(1,0,0);cgflip('v');
            
            
        end
    end
    
end
% toc

% cgshut
%===========================================================================
%===========================================================================
%===========================================================================







