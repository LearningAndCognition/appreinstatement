function varargout =  p_stim_cog_coherenz(monfreq,time2play,Nstar,NpixTFT,...
    Pcoh,xcohdir,ycohdir,STAT,colstar,dio, LPTpin1,LPTpin2);
%===========================================================================
% function varargout =  p_stim_cog_coherenz(monfreq,time2play,Nstar,NpixTFT,...
%     Pcoh,xcohdir,ycohdir,STAT,colstar,LPTpin1,LPTpin2);
%===========================================================================
% %   %OUT: t0=rel. timestamp when motion begins
%     monfreq=56    %(Hz):    ;
%     time2play= 3 % duration(ms)
%     Nstar=26000%      number 0f stars(26000)
%     NpixTFT=2000%        size of whole screen    (2000)
%     Pcoh=50%:       percentual-coherence ;
%     xcohdir=1%:    x coherence direction    (1)
%     ycohdir=0%    y coherence direction    (1)
%     STAT=1%      =static starfield vs moving vs blak screen [0/1/-1]
%     colstar: colr of stars def.=[1 1 1] ;simulate also blackscren [0 0 0] 
%      with the same timing (don't use that)
%      dio= LPThandle /object to call (dio)
%      LPTpin1 :pin for LPT1 ONSET of moving 
%      LPTpin1 :pin for LPT2 END of moving 

%===========================================================================
timcyc2= floor(((time2play*2/5)/1000)  /  (1/monfreq));
NincohSTARS=Nstar-Nstar*Pcoh/100; %Number of incoherent stars
x=(rand(1,Nstar)*NpixTFT-NpixTFT/2)';%sets Stars
y=(rand(1,Nstar)*NpixTFT-NpixTFT/2)';
xrand=1:length(x);
yrand=1:length(y); 
Dirpossib=[-1 -1;1 1;-1 1;1 -1;0 -1; 0 1; -1 0; 1 0]; %possible directions without static [0 0]
Dirpossib=repmat(Dirpossib,ceil(NincohSTARS/8),1);
xymoverand=shuffle([Dirpossib ; [ones(Nstar-NincohSTARS,1)*xcohdir  ones(Nstar-NincohSTARS,1)*ycohdir] ]);
w(1:Nstar)=3;%size of stars
h(1:Nstar)=3;

%prepares movie for movingstars (STAT=1)
matrMOVx=repmat(round(x),1,timcyc2);
matrMOVy=repmat(round(y),1,timcyc2);
if STAT==1
    for i= 2:1:timcyc2
        matrMOVx(:,i)= xymoverand(:,1)+ matrMOVx(:,i-1);
        matrMOVy(:,i)= xymoverand(:,2)+ matrMOVy(:,i-1);
    end
end

%----------------start
clear xrand yrand Dirpossib x y xymoverand;
%===========================================================================
% tic

putvalue(dio,LPTpin1); wait(15); putvalue(dio,0); %pin2EEG
varargout{1}=time;  
if STAT~=1; clearkeys;end  %if movingstars clear keyboradbuffer

for i=1:timcyc2
    if time<varargout{1}+time2play
        %         x(1:Nstar)= x(1:Nstar)+xymoverand(:,1); y(1:Nstar)= y(1:Nstar)+xymoverand(:,2);  
        %         cgpencol(1,1,1);cgellipse(x,y,w,h,'f');
        cgpencol(colstar(1),colstar(2),colstar(3));
        %cgellipse(matrMOVx(:,i),matrMOVy(:,i),w,h,'f');
        cgrect(matrMOVx(:,i),matrMOVy(:,i),w,h);
        
        cgpencol(0,0,0);cgrect(0,0,100,600);
        cgpencol(1,0,0);cgpenwid(1);cgdraw(0,5,0,-5);cgdraw(5,0,-5,0);
        cgflip(0,0,0);
%         i
    end
  end
  %putvalue(dio,LPTpin2); wait(5); putvalue(dio,0); %pin2EEG
  
 