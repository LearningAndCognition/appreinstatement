#include <windows.h>
#include <string.h>
#include "resource.h"

char szDriverFileName[MAX_PATH];
char DriverName[]="UserPort";
HWND hDlg;

unsigned Chars2Hex (char ch, unsigned oldval)
{
  int nHex;

  if ((ch>='0') && (ch<='9'))
    nHex=ch-'0';
  else if ((ch>='a') && (ch<='f'))
    nHex=ch-'a'+10;
  else if ((ch>='A') && (ch<='F'))
    nHex=ch-'A'+10;
  else
    return oldval;
  return oldval*16+nHex;
}

void UpperString(char *Str)
{
  int i,Len;

  Len=strlen(Str);
  for (i=0;i<Len;i++) {
    if (Str[i]>'Z') {
      Str[i]-=32;
    }
  }
}

BOOL StopDriver()
{
  SC_HANDLE  schService;
  SC_HANDLE   schSCManager;
  SERVICE_STATUS  serviceStatus;
  UCHAR IOPM[0x2000];
  DWORD cb1,wType;
  HKEY hKey;
  BOOL bResult1=FALSE;

  if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\UserPort",0,KEY_READ, &hKey) == ERROR_SUCCESS) {
    cb1 = sizeof(IOPM);
    if (RegQueryValueEx(hKey,"IOPM",0,&wType,(BYTE *)IOPM,&cb1) == ERROR_SUCCESS)
      bResult1 = TRUE;
    RegCloseKey(hKey);
  }

  schSCManager = OpenSCManager (NULL,   // machine (NULL == local)
                                NULL,   // database (NULL == default)
                                SC_MANAGER_ALL_ACCESS // access required
                               );
  if (schSCManager == NULL) {
    return FALSE;
  }

  schService = OpenService (schSCManager,DriverName,SERVICE_ALL_ACCESS);
  if (schService == NULL) {
    CloseServiceHandle (schSCManager);
    return FALSE;
  }

  ControlService (schService, SERVICE_CONTROL_STOP, &serviceStatus);

  DeleteService (schService);

  CloseServiceHandle (schService);
  CloseServiceHandle (schSCManager);

  Sleep(200);
  if (bResult1 == TRUE) {
    if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\UserPort",0,"", REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS, NULL,&hKey,&wType) == ERROR_SUCCESS) {
      RegSetValueEx(hKey, "IOPM",0,REG_BINARY, (BYTE *)IOPM,sizeof(IOPM));
      RegCloseKey(hKey);
    }
  }

  return TRUE;
}

BOOL StartDriver()
{
  SC_HANDLE schService = NULL;
  SC_HANDLE schSCManager;
  HANDLE   hDriver;
  DWORD LastError;
  char szMess[300];
  char szTmp[MAX_PATH];
	BOOL res;

  lstrcpy(szTmp,szDriverFileName);
  szTmp[12] = '\0';
  if (lstrcmpi(szTmp,"\\SystemRoot\\")==0) {
    GetWindowsDirectory(szTmp,sizeof(szTmp));
    lstrcat(szTmp,szDriverFileName+11);
  }
  else {
    lstrcpy(szTmp,szDriverFileName);
  }


  hDriver = CreateFile(szTmp, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  if (hDriver==INVALID_HANDLE_VALUE) {
    wsprintf(szMess,"Driver does not exist:\r\n%s",szTmp);
    MessageBox(hDlg,szMess,"UserPort",MB_OK);
    return FALSE;
  }

  CloseHandle(hDriver);

  schSCManager = OpenSCManager(NULL,   // machine (NULL == local)
                               NULL,   // database (NULL == default)
                               SC_MANAGER_ALL_ACCESS // access required
                              );

  if (schSCManager == NULL) {
    if (GetLastError() ==  ERROR_ACCESS_DENIED)
      MessageBox(hDlg,"You are not authorized to install drivers.\r\nPlase contact your administrator.","UserPort",MB_OK);
    else
      MessageBox(hDlg,"Unable to start driver!","UserPort",MB_OK);

    return FALSE;
  }

  schService = CreateService(schSCManager, // SCManager database
                             DriverName,   // name of service
                             DriverName,   // name to display
                             SERVICE_START,//SERVICE_ALL_ACCESS,    // desired access
                             SERVICE_KERNEL_DRIVER, // service type
                             SERVICE_SYSTEM_START,  // start type
                             SERVICE_ERROR_NORMAL,  // error control type
                             szDriverFileName,    // service's binary
                             NULL,    // no load ordering group
                             NULL,    // no tag identifier
                             NULL,    // no dependencies
                             NULL,    // LocalSystem account
                             NULL     // no password
                            );

  if (schService == NULL) {
    LastError = GetLastError();
    if (LastError == ERROR_SERVICE_EXISTS)
      MessageBox(hDlg,"Driver already started!","UserPort",MB_OK);
    else if (LastError  ==  ERROR_ACCESS_DENIED)
      MessageBox(hDlg,"You are not authorized to install drivers.\r\nPlase contact your administrator.","UserPort",MB_OK);
    else
      MessageBox(hDlg,"Unable to start driver!","UserPort",MB_OK);

    CloseServiceHandle(schSCManager);
    return FALSE;
  }

  res = StartService (schService,    // service identifier
                0,             // number of arguments
                NULL           // pointer to arguments
               );

  CloseServiceHandle(schService);
  CloseServiceHandle(schSCManager);
	if (!res)	{
    MessageBox(hDlg,"Unable to start driver service!","UserPort",MB_OK);
    return FALSE;
	}
  return TRUE;
}

BOOL IsDriverStarted()
{
  SC_HANDLE schService = NULL;
  SC_HANDLE schSCManager;

  schSCManager = OpenSCManager(NULL,   // machine (NULL == local)
                               NULL,   // database (NULL == default)
                               SC_MANAGER_ALL_ACCESS // access required
                              );

  if (schSCManager == NULL) {
    return FALSE;
  }

  schService = OpenService(schSCManager, // SCManager database
                           DriverName,   // name of service
                           SERVICE_START//SERVICE_ALL_ACCESS,    // desired access
                          );

  if (schService == NULL) {
    return FALSE;
  }

  CloseServiceHandle(schService);
  CloseServiceHandle(schSCManager);
  return TRUE;
}

BOOL GetStartAndStopAddress(char *szStr, unsigned *nStartAddress, unsigned *nStopAddress)
{
  unsigned i, nStart = 0, nStop = 0;

  for (i=0;(szStr[i]!='\0')&&(szStr[i]!='-');i++)
    nStart = Chars2Hex(szStr[i], nStart);

  if (szStr[i]=='\0')
    return FALSE;

  for (i++;(szStr[i]!='\0')&&(szStr[i]!='-');i++)
    nStop = Chars2Hex(szStr[i], nStop);

  *nStartAddress = nStart;
  *nStopAddress = nStop;

  if (nStop < nStart)
    return FALSE;
  if (nStop > 0xffff)
    return FALSE;

  return TRUE;
}

BOOL AddAUMPBtn()
{
  char szTemp[256];
  char szTemp2[256];
  unsigned nStartAddress,nStopAddress;

  GetDlgItemText(hDlg,IDC_AUMP_EDIT_ADD,szTemp,sizeof(szTemp));

  if (!GetStartAndStopAddress(szTemp, &nStartAddress, &nStopAddress)) {
    MessageBox(hDlg,"Wrong syntax. Use: \"startadress - stopaddress\" (0000 - ffff)","UserPort",MB_OK);
    return FALSE;
  }
  if (nStopAddress > 0xffff) {
    MessageBox(hDlg,"Wrong syntax. Use: \"startadress - stopaddress\" (0000 - ffff)","UserPort",MB_OK);
    return FALSE;
  }

  wsprintf(szTemp2,"%X - %X",nStartAddress,nStopAddress);
  SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_ADDSTRING, 0,(LPARAM)szTemp2);
  SetDlgItemText(hDlg,IDC_AUMP_EDIT_ADD,"");
  return TRUE;
}

BOOL UpdateBtn()
{
  HKEY hKey;
  DWORD wType;
  UCHAR IOPM[0x2000];
  unsigned  nCount,i,j;
  char szTemp[256];
  unsigned nStartAddress,nStopAddress;

  FillMemory(IOPM,sizeof(IOPM),0xFF);

  nCount = SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_GETCOUNT, 0,0);

  for (i=0;i<nCount;i++) {
    SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_GETTEXT, i,(LPARAM)szTemp);
    if (GetStartAndStopAddress(szTemp, &nStartAddress, &nStopAddress)) {
      for (j=nStartAddress;j<=nStopAddress;j++)
        IOPM[j>>3] &= ~(1 << (j&7));
    }
  }

  if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\UserPort",0,"", REG_OPTION_NON_VOLATILE,KEY_READ|KEY_WRITE, NULL,&hKey,&wType) == ERROR_SUCCESS) {
    RegSetValueEx(hKey, "IOPM",0,REG_BINARY, (BYTE *)IOPM,sizeof(IOPM));

    RegCloseKey(hKey);
    if (StopDriver()) {
      Sleep(200);
      StartDriver();
    }
    return TRUE;
  }

  MessageBox(hDlg,"Could not write to the registry key:\r\n"
                  "HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\UserPort\r\n\r\n" 
                  "You are not authorized to install drivers.\r\nPlase contact your administrator.","UserPort",MB_OK);

  return FALSE;
}

BOOL ReadRegistry()
{
  UCHAR IOPM[0x2000];
  char szTemp[256];
  unsigned i,nStartAddress,nStopAddress;
  DWORD cb,wType, bResult1 = FALSE;
  HKEY hKey;

  FillMemory(IOPM,sizeof(IOPM),0xFF);

  if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\UserPort",0,KEY_READ, &hKey) == ERROR_SUCCESS) {
    cb = sizeof(IOPM);
    if (RegQueryValueEx(hKey,"IOPM",0,&wType,(BYTE *)IOPM,&cb) == ERROR_SUCCESS)
      bResult1 = TRUE;
    RegCloseKey(hKey);
  }

  if (bResult1 == FALSE)
    return FALSE;

  for (i=0;i <sizeof(IOPM)<<3;i++) {
    if ((IOPM[i>>3] & (1 << (i&7))) == 0) {
      nStartAddress = i;
      do {
        nStopAddress = i;
        i++;
      } while ((i < sizeof(IOPM)<<3) && ((IOPM[i>>3] & (1 << (i&7))) == 0));
      wsprintf(szTemp,"%X - %X",nStartAddress,nStopAddress);
      SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_ADDSTRING, 0,(LPARAM)szTemp);
    }
  }

  return TRUE;
}

BOOL AddAUMPDefaults()
{
  int nCount,i;
  
  nCount = SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_GETCOUNT, 0,0);

  for (i=0;i<nCount;i++)
    SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_DELETESTRING, 0,0);

  SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_ADDSTRING, 0,(LPARAM)"200 - 37F");
  SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_ADDSTRING, 0,(LPARAM)"3BC - 3BF");
  SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_ADDSTRING, 0,(LPARAM)"3E8 - 3FF");
  return TRUE;
}

void outportb(UINT portid, BYTE value)
{
  __asm mov edx,portid
  __asm mov al,value
  __asm out dx,al
}

BYTE inportb(UINT portid)
{
  unsigned char value;
  
  __asm mov edx,portid
  __asm in al,dx
  __asm mov value,al
  return value;
}

void TestLpt1(BOOL bOpenTFC)
{
  BYTE value;
  HANDLE hUserPort;

  if (MessageBox(hDlg,"UserPort will now reset your printer.\r\n\r\nMake sure that you have started the driver and that you have\r\nenabled port 37A before you try resetting the printer.","UserPort",MB_OKCANCEL)==IDCANCEL)
    return;

  if (bOpenTFC) {
    hUserPort = CreateFile("\\\\.\\UserPort", GENERIC_READ, 0, NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hUserPort==INVALID_HANDLE_VALUE) {
      MessageBox(hDlg,"Could not open \\\\.\\UserPort\r\n\r\nPlease start the driver first!","UserPort",MB_OK);
      return;
    }
  }

  value = inportb(0x37a);
  value = value & 0xfb;
  outportb(0x37a,value);
  Sleep(100);
  value = value | 0x04;
  outportb(0x37a,value);
  CloseHandle(hUserPort);
}

void play(int hz, int duration)
{
  outportb(0x61, (BYTE)(inportb(0x61) | 0x03));
  hz = 1193180 / hz;
  outportb(0x43, 0xb6);
  outportb(0x42, (BYTE)hz);
  outportb(0x42, (BYTE)(hz >> 8));
  Sleep(duration);
  outportb(0x61, (BYTE)(inportb(0x61) & ~0x03) );
}

void TestSpkr(BOOL bOpenTFC)
{
  HANDLE hUserPort;

  if (MessageBox(hDlg,"UserPort will now play a small tune on the internal speaker.\r\n\r\nMake sure that you have started the driver and that you have\r\nenabled port 42,43 and 61 before you try playing the tune.","UserPort",MB_OKCANCEL)==IDCANCEL)
    return;

  if (bOpenTFC) {
    hUserPort = CreateFile("\\\\.\\UserPort", GENERIC_READ, 0, NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hUserPort==INVALID_HANDLE_VALUE) {
      MessageBox(hDlg,"Could not open \\\\.\\UserPort\r\n\r\nPlease start the driver first!","UserPort",MB_OK);
      return;
    }
  }

  Sleep(100);
  play(900,  500);
  play(1000, 500);
  play(800,  500);
  play(400,  500);
  play(600,  1000);
  CloseHandle(hUserPort);
}

BOOL WINAPI DlgProc(HWND   hWnd,    //Handtag till f�nster, 16-bitar
                    UINT   msg,     //Meddelande, unsigned int
                    WPARAM wParam,  //Parameter (word), unsigned int
                    LPARAM lParam ) //Parameter (long), unsigned long int
{
  char szTemp[256];
  int nIndex;
  BOOL result;
  switch (msg)
  {
    case WM_INITDIALOG:
      hDlg=hWnd;
      if (!ReadRegistry()) {
        AddAUMPDefaults();
      }
      result = IsDriverStarted();
      EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_START), !result);
      EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_STOP), result);
      break;
    case WM_COMMAND:
      switch (LOWORD(wParam))
      {
        case IDCANCEL:
          PostQuitMessage( 0 );
          EndDialog(hWnd,FALSE);
          break;
        case IDC_BUTTON_START:
          if (StartDriver()) {
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_START), FALSE);
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_STOP), TRUE);
          }
          else {
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_START), TRUE);
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_STOP), TRUE);
          }
          break;
        case IDC_BUTTON_STOP:
          if (StopDriver())
          {
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_START), TRUE);
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_STOP), FALSE);
          }
          else 
          {
            if (GetLastError() ==  ERROR_ACCESS_DENIED)
              MessageBox(hDlg,"You are not authorized to remove drivers.\r\nPlase contact your administrator.","UserPort",MB_OK);
            else
              MessageBox(hDlg,"Driver is not installed!","UserPort",MB_OK);
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_START), TRUE);
            EnableWindow(GetDlgItem(hDlg,IDC_BUTTON_STOP), TRUE);
          }
          break;
        case IDC_AUMP_BUTTON_ADD:
          AddAUMPBtn();
          break;
        case IDC_BUTTON_UPDATE:
          UpdateBtn();
          break;
        case IDC_AUMP_BUTTON_REMOVE:
          nIndex = SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_GETCURSEL, 0,0);
          SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_DELETESTRING, nIndex,0);
          EnableWindow(GetDlgItem(hWnd,IDC_AUMP_BUTTON_REMOVE), FALSE);
          break;
        case IDC_AUMP_BUTTON_DEFAULTS:
          AddAUMPDefaults();
          break;
        case IDC_AUMP_EDIT_ADD:
          GetDlgItemText(hDlg,IDC_AUMP_EDIT_ADD,szTemp,sizeof(szTemp));
          EnableWindow(GetDlgItem(hWnd,IDC_AUMP_BUTTON_ADD), szTemp[0]!='\0');
          break;
        case IDC_AUMP_BUTTON_TESTLPT1:
          TestLpt1(TRUE);
          break;
        case IDC_AUMP_BUTTON_TESTSPKR:
          TestSpkr(TRUE);
          break;
        case IDC_AUMP_LIST_GRANTS:
          if (HIWORD(wParam) == LBN_SELCHANGE) {
            nIndex = SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_GETCURSEL, 0,0);
            SendDlgItemMessage(hDlg, IDC_AUMP_LIST_GRANTS,LB_SETCURSEL, nIndex,0);
            EnableWindow(GetDlgItem(hWnd,IDC_AUMP_BUTTON_REMOVE), TRUE);
          }
          break;
        default:
          return FALSE;
          break;
      } 
      break;
    default:
      return FALSE;
  }
  return TRUE;
} //DlgProc

int WINAPI WinMain(
  HINSTANCE hInstance,
  HINSTANCE hPrevInstance,
  LPSTR lpszCmdline,
  int nShow )
{
  MSG msg;
  OSVERSIONINFO osvi;

  FillMemory(&osvi,sizeof(osvi),0);
  osvi.dwOSVersionInfoSize = sizeof(osvi);
  
  GetVersionEx(&osvi);

  if ((osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) || (osvi.dwPlatformId == VER_PLATFORM_WIN32s)) {
    MessageBox(NULL,"UserPort does not work on Windows 3.11/95/98/Me.\r\n\r\nUsermode programs does always have access to ports in Windows 3.11/95/98.\r\nThis program is therefore not needed on these operating systems.","UserPort",MB_OK);
    return 0;
  }

  if (lpszCmdline[0] != '\0')
    lstrcpy(szDriverFileName, lpszCmdline);
  else
    lstrcpy(szDriverFileName,"\\SystemRoot\\System32\\Drivers\\UserPort.sys");
  CreateDialog(hInstance, MAKEINTRESOURCE(IDD_USERPORT), NULL, (DLGPROC)DlgProc);

  SetClassLong(hDlg,GCL_HICON,(long)LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APP)));

  while ( GetMessage ( &msg, NULL, 0, 0 ) )
  {
    if (IsDialogMessage(hDlg,&msg))
      continue;
    TranslateMessage ( &msg );
    DispatchMessage ( &msg );
  }
  return 0;
}  //WinMain
