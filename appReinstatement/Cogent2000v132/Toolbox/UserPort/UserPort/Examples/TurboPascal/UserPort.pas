uses crt;

procedure outportb(portid : integer; value : BYTE);
Begin
  asm
    mov dx,portid
    mov al,value
    out dx,al
  end;
end;

function inportb(portid : integer) : byte;
Var value : byte;
Begin
  asm
    mov dx,portid
    in al,dx
    mov value,al
  end;
  inportb := value;
end;

var
  value : integer;
begin
  value := inportb($37a);
  value := value and $fb;
  outportb($37a,value);
  Delay(100);
  value := value or $04;
  outportb($37a,value);
end.