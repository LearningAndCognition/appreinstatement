unit DemoAppFrm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,IOPort;

type
  TForm1 = class(TForm)
    ResetPrinter: TButton;
    procedure ResetPrinterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.ResetPrinterClick(Sender: TObject);
var
  value : integer;
begin
  value := inportb($37a);
  value := value and $fb;
  outportb($37a,value);
  Sleep(100);
  value := value or $04;
  outportb($37a,value);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
end;

end.
