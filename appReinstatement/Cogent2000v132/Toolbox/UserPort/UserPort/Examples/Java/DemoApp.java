class DemoApp {
  public static void main(String[] args) {
    UserPort up;
    up = new UserPort();

    byte value;

    value = up.inportb(0x37a);
    value = (byte)((int)value & 0xfb);
    up.outportb(0x37a,value);
    try {
      Thread.sleep(100);
    } catch(Exception e) {
      // Do nothing
    }
    value = (byte)((int)value | 0x04);
    up.outportb(0x37a,value);
  }
}

