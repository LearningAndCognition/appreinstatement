It is not possible to access UserPort directly from within Java
because it is not possible to use inline assembler in Java.
The solution is to use Java native methods and use assembler in C.
This directory contains an example of how to do this. This example
is based on the sun tutorial:
http://java.sun.com/docs/books/tutorial/native1.1/index.html

Please run the Makefile.bat to compile the test program.
(You probably need to change some paths in the Makefile.bat)