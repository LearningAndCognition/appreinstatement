#ifndef IOPORTH
#define IOPORTH

#ifdef __cplusplus
extern "C" {
#endif

void outport(unsigned int portid, unsigned int value);
void outportb(unsigned int portid, unsigned char value);
unsigned char inportb(unsigned int portid);
unsigned int inport(unsigned int portid);
int OpenThroughUserPortProcessAccess();
int CloseThroughUserPortProcessAccess();

#ifdef __cplusplus
}
#endif

#endif
