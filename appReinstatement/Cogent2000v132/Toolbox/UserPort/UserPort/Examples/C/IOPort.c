#include <windows.h>

void outport(unsigned int portid, unsigned int value)
{
  __asm mov edx,portid;
  __asm mov eax,value;
  __asm out dx,ax;
}
void outportb(unsigned int portid, unsigned char value)
{
  __asm mov edx,portid
  __asm mov al,value
  __asm out dx,al
}

unsigned char inportb(unsigned int portid)
{
  unsigned char value;
  
  __asm mov edx,portid
  __asm in al,dx
  __asm mov value,al
  return value;
}

unsigned int inport(unsigned int portid)
{
  int value=0;
  __asm mov edx,portid
  __asm in ax,dx
  __asm mov value,eax
  return value;
}

