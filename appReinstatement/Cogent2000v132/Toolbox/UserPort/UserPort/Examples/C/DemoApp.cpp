#include <windows.h>
#include "IOPort.h"

int main()
{
  int value;

  value = inportb(0x37a);
  value = value & 0xfb;
  outportb(0x37a,value);
  Sleep(100);
  value = value | 0x04;
  outportb(0x37a,value);

  return 0;
}