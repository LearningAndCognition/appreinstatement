#include <windows.h>
static HANDLE hUserPort = INVALID_HANDLE_VALUE;

void WINAPI outport(unsigned int portid, unsigned int value)
{
  __asm mov edx,portid;
  __asm mov eax,value;
  __asm out dx,ax;
}
void WINAPI outportb(unsigned int portid, unsigned char value)
{
  __asm mov edx,portid
  __asm mov al,value
  __asm out dx,al
}

unsigned char WINAPI inportb(unsigned int portid)
{
  unsigned char value;
  
  __asm mov edx,portid
  __asm in al,dx
  __asm mov value,al
  return value;
}

unsigned int WINAPI inport(unsigned int portid)
{
  int value=0;
  __asm mov edx,portid
  __asm in ax,dx
  __asm mov value,eax
  return value;
}

int WINAPI OpenThroughUserPortProcessAccess()
{
  hUserPort = CreateFile("\\\\.\\UserPort", GENERIC_READ, 0, NULL,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
  return (hUserPort != INVALID_HANDLE_VALUE);
}

int WINAPI CloseThroughUserPortProcessAccess()
{
  if (hUserPort != INVALID_HANDLE_VALUE) {
    CloseHandle(hUserPort);
  }
  return (hUserPort != INVALID_HANDLE_VALUE);
}
