It is not possible to access UserPort directly from Visual Basic
because it is not possible to use inline assembler in Visual Basic.
The solution is to call a the UserPort.DLL which is written in C.
