Attribute VB_Name = "UserPort"
Public Declare Function EnumWindows Lib "user32" (callback As Byte, param As Long) As LogModeConstants
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Integer)

'long WINAPI outport(long hWnd, long *param)
'{
'8B 4C 24 08: mov ecx,dword ptr [esp+0Ch]
'8B 11      : mov edx,dword ptr [ecx]
'8B 41 04   : mov eax,dword ptr [ecx+4]
'EE         : out dx,al
'33 C0      : xor eax,eax
'C2 08 00   : ret 8
'}

Public Sub outportb(ByVal portid As Integer, ByVal value As Byte)
  Dim param(2) As Long
  Dim outport_code(14) As Byte
  
  outport_code(0) = &H8B
  outport_code(1) = &H4C
  outport_code(2) = &H24
  outport_code(3) = &H8
  outport_code(4) = &H8B
  outport_code(5) = &H11
  outport_code(6) = &H8B
  outport_code(7) = &H41
  outport_code(8) = &H4
  outport_code(9) = &HEE
  outport_code(10) = &H33
  outport_code(11) = &HC0
  outport_code(12) = &HC2
  outport_code(13) = &H8
  outport_code(14) = &H0

  param(0) = portid
  param(1) = value And &HFF
  EnumWindows outport_code(0), param(0)
End Sub

'long WINAPI inport(long hWnd, long *param)
'{
'8B 4C 24 08: mov ecx,dword ptr [esp+0Ch]
'8B 11      : mov edx,dword ptr [ecx]
'EC         : in  al,dx
'89 41 04   : mov dword ptr [ecx+4],eax
'33 C0      : xor eax,eax
'C2 08 00   : ret 8
'}

Public Function inportb(ByVal portid As Integer) As Byte
  Dim param(2) As Long
  Dim inport_code(14) As Byte
  
  inport_code(0) = &H8B
  inport_code(1) = &H4C
  inport_code(2) = &H24
  inport_code(3) = &H8
  inport_code(4) = &H8B
  inport_code(5) = &H11
  inport_code(6) = &HEC
  inport_code(7) = &H89
  inport_code(8) = &H41
  inport_code(9) = &H4
  inport_code(10) = &H33
  inport_code(11) = &HC0
  inport_code(12) = &HC2
  inport_code(13) = &H8
  inport_code(14) = &H0
  
  param(0) = portid
  EnumWindows inport_code(0), param(0)
  inportb = param(1) And &HFF
End Function

