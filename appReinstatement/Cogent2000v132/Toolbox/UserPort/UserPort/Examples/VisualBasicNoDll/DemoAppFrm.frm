VERSION 5.00
Begin VB.Form DemoApp 
   Caption         =   "Demo App"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton ResetPrinter 
      Caption         =   "Reset Printer"
      Height          =   615
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   1695
   End
End
Attribute VB_Name = "DemoApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub ResetPrinter_Click()
  Dim value As Byte
  
  value = inportb(&H37A)
  value = value And &HFB
  outportb &H37A, value
  Sleep &H30
  value = value Or &H4
  outportb &H37A, value

End Sub
