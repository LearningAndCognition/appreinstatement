It is normally not possible to access UserPort directly from Visual Basic
because it is not possible to use inline assembler in Visual Basic.
This program shows how this can be done by letting EnumWindows call
our machine code as a callback and hence our IN and OUT assembler
instructions can be called from Visual Basic. This makes it possible
for you to ship your product without the DLL.
